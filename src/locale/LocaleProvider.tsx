import { useState, createContext,useEffect, useContext } from "react";
import uzLatinLangs from "./languages/uz-latin.json";
import ruLangs from "./languages/ru.json";
import { MainPropType } from "../shared/types";
 
export type LangType = "uz" | "ru";

const Context = createContext<undefined | any>(undefined);

export const useLocale = () => {
  return useContext(Context);
};

function LocaleProvider({
  children,
  lang   ,
}: { lang?: LangType } & MainPropType) {

  let currentLanguage  =   localStorage.getItem("language") as  LangType | null   
  if ( !currentLanguage) {
    currentLanguage = "uz"
    localStorage.setItem("language", 'uz');
  }

  const langsObject  = currentLanguage ===  "uz" ? uzLatinLangs : ruLangs

  const [locale, setLocale] = useState<{ lang: LangType; langsObject: any }>({
    lang:currentLanguage,
    langsObject ,
  });

  
  

  const setLocaleLanguage = (lang: LangType) => {
     localStorage.setItem("language", lang);
    switch (lang) {
      case "uz":
        setLocale((prev) => ({ ...prev, lang, langsObject: uzLatinLangs }));
        break;
      case "ru":
        setLocale((prev) => ({ ...prev, lang, langsObject: ruLangs }));
        break;

      default:
        break;
    }
  };
  const t = (id: string) => {
    return locale.langsObject[id];
  };
  //   const getQueryLang = (ru:string,uz:string) => {
  //     return locale.lang === "uz" ? uz : ru
  //   };
   
  return (
    <Context.Provider value={{ setLocaleLanguage, t, lang: locale.lang }}>
      {children}
    </Context.Provider>
  );
}

export default LocaleProvider;
