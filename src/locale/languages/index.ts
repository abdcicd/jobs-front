import uzLatinLangs from "./uz-latin.json";
import ruLangs from "./ru.json";
import { LangType } from "../LocaleProvider";

const languages = {
  uz: uzLatinLangs,
  ru: ruLangs,
};

export const t = (id: any) => {
  const lang = localStorage.getItem("language") as LangType;
  const currentList = languages[lang];

  return lang && languages[lang][id as keyof typeof currentList];
};
