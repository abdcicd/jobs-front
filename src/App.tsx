import { ConfigProvider } from "antd";
import JWTAuthAuthProvider from "./auth/JWTAuthProvider";
import AppLayout from "./layout";
import { BrowserRouter } from "react-router-dom";
import "./shared/constants";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import { QueryClient, QueryClientProvider } from "react-query";
import LocaleProvider from "./locale/LocaleProvider";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      retry: false,
    },
  },
});

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <BrowserRouter>
        <LocaleProvider>
          <JWTAuthAuthProvider>
            <ConfigProvider
              theme={{
                token: {
                  colorPrimary: "#fb3f00",
                },
              }}
            >
              <AppLayout />
            </ConfigProvider>
          </JWTAuthAuthProvider>
        </LocaleProvider>
      </BrowserRouter>
    </QueryClientProvider>
  );
}

export default App;
