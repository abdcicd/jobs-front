import { authRouteConfig } from "./auth";
import { errorRouteConfig } from "./errorPages";
import { profileRouteConfig } from "./profile";
import { sampleRouteConfig } from "./sample";

const unAuthorizedStructure = {
   routes: sampleRouteConfig.concat(errorRouteConfig,authRouteConfig),
};


const authorizedStructure = {
   routes: sampleRouteConfig.concat(errorRouteConfig,profileRouteConfig),
};

export { unAuthorizedStructure,authorizedStructure };
