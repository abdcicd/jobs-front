export const navLinks = [
  {
    key: 1,
    to: "/home",
    children: "navbar.home",
  },
  {
    key: 2,
    to: "/about",
    children: "navbar.about",
  
  },
  {
    key: 3,
    to: "/vacancies",
    children: "navbar.vacancies",
  },
  {
    key: 4,
    to: "/companies",
    children: "navbar.companies",
  },
];
