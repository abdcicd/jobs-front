import React from 'react'
import image from "../../../assets/images/comingsoon.png"

function ComingSoon() {
  return (
    <div style={{ height: "calc(100vh - 89px)",display:"flex",alignItems:"center",justifyContent:"center",flexDirection:"column" }}>
    <img
      src={image}
      alt=""
      style={{
        maxWidth: 600,
          width: "100%",
         objectFit: "contain",
        mixBlendMode: "darken",
      }}
    /> 
    <p style={{color:"grey",marginTop:20,textAlign:"center",lineHeight:"160%",maxWidth:500}}>The page you are looking for is now building. You can see it as soon as it's done!</p>
  </div>  )
}

export default ComingSoon