import { RouteType } from '../../shared/types';
import ComingSoon from './ComingSoon';
import Error404 from './Error404';
 

export const errorRouteConfig : RouteType[] = [
  {
    path: '/coming-soon',
    element: <ComingSoon/>,
  },
  {
    path: '/not-found',
    element: <Error404/>,
  },
 
   
];
