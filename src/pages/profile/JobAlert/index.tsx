 import Title from "../../../components/Title";
import Notfications from "../../../components/sections/Notfications";
import image from "../../../assets/images/majlis.png";

function JobAlert() {
  const alerts = Array.from({ length: 3 });
  return (
    <div
      style={{
        maxWidth: 800,
        padding: "70px 0",
        margin: "0 auto",
      }}
    > 
    
      {!alerts.length ? (
        <img
          src={image}
          alt=""
          style={{
            width: "100%",
            height: "calc(100vh - 280px)",
            objectFit: "contain",
            maxWidth: 600,
            mixBlendMode: "darken",
            margin: "auto",
          }}
        />
      ) : (
        <>
          <Title>Notfications</Title>

          <Notfications alerts={alerts} />
        </>
      )}{" "}
    </div>
  );
}

export default JobAlert;
