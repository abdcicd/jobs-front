import React, { useState } from "react";
import {
  UserActionsType,
  useAuthAction,
  useAuthData,
} from "../../../auth/JWTAuthProvider";
import {
  Button,
  Checkbox,
  Modal,
  Select,
  Space,
  Spin,
  Table,
  notification,
} from "antd";
import { Link, Navigate, useLocation, useParams } from "react-router-dom";
import styles from "./index.module.scss";
import Title from "../../../components/Title";
import MyButton from "../../../components/Button";
import { UserType } from "../../../shared/types";
import { getColumns } from "./columns";
import {
  DeleteOutlined,
  EditTwoTone,
  ExclamationCircleTwoTone,
} from "@ant-design/icons";
import jwtAxios from "../../../auth/jwt-api";
import { useLocale } from "../../../locale/LocaleProvider";

export type PageType =
  | "education"
  | "languages"
  | "work_experience"
  | "recommandations"
  | "recommender_employees"
  | "relative_employees";

export const title = {
  education: "Education",
  languages: "Languages",
  work_experience: "Experience",
  recommandations: "Recomendations",
  recommender_employees: "Recommender peoples",
  relative_employees: "Relative employies",
};
export const peoplePages = [
  "recommandations",
  "recommender_employees",
  "relative_employees",
];

function TablePage() {
  const { user } = useAuthData();
  const { setUserData } = useAuthAction();
  const { search } = useLocation();
  const queryParams = new URLSearchParams(search);
  const page = queryParams.get("page") as PageType;
  const { confirm } = Modal;
  const {t} = useLocale()

  
  const deleteByIndex = (index: number) => {
    confirm({
      title: t("modal.delete") ,
      icon: <ExclamationCircleTwoTone twoToneColor="red" />,
      okText:  t(`button.yes`),
      okType: "danger",
      cancelText: t(`button.no`),
      async onOk() {
        try {
          const userData = [...(user?.[page] || [])].filter(
            (_: any, i: number) => i !== index
          );
          const res = await jwtAxios.put("/web/profile", { [page]: userData });

          setUserData((prev: UserActionsType) => ({ ...prev, user: res.data }));

          notification.success({
            message:   t("notification.success.delete"),
          });
        } catch (error) {  notification.error({
          message:   t("notification.error.delete"),
        });
          console.log(error);
        }
      },
    });
  };
  const columns = getColumns(t,deleteByIndex)[page];

  return (
    <div className={styles["table-component"]}>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          gap: 30,
          flexWrap: "wrap",
          margin: "30px 0 50px",
        }}
      >
        <div>
          <Title>{t(`profile.${page}`)}</Title>
          <p
           className="title-page-description"
          >
            {t(`profile.description.template`).replace(
              "{%%}",
              t(`profile.${page}`)
            )}
          </p>{" "}
        </div>
        <Link
          to={`/profile/edit/${
            peoplePages.includes(page) ? `people?field=${page}` : page
          }`}
        >
          <MyButton mode="outline" style={{ padding: "10px 20px" }}>
            {t("button.add")}
          </MyButton>
        </Link>
      </div>
      <Table
        dataSource={user?.[page] || ([] as any[])}
        columns={columns}
        scroll={{ x: "100%" }}
      />
    </div>
  );
}

export default TablePage;
