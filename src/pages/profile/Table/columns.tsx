import { Button, Checkbox, Space } from "antd";
import { Link } from "react-router-dom";
import { RelativeType } from "../../../shared/types";
import AddSertificate from "../../../components/AddSertificate";

const fixed: "right" = "right";

export const getColumns = (t: any, deleteByIndex?: any) => ({
  education: [
    {
      title: t("profile.school"),
      dataIndex: "school",
      key: "school",
      //   width: 300,
    },
    {
      title: t("profile.degree"),
      dataIndex: "degree_name",
      key: "degree_name",
    },
    {
      title: t("profile.field-of-study"),
      dataIndex: "field_of_study",
      key: "field_of_study",
      //   render: (vacancy: VacancyType) => vacancy.uuid,
    },
    {
      title: t("profile.start-date"),
      dataIndex: "start",
      key: "start",
    },
    {
      title: t("profile.end-date"),
      dataIndex: "end",
      key: "end",
    },
    {
      title: t("profile.present"),
      dataIndex: "to_present",
      key: "to_present",

      render: (present: boolean) => {
        return (
          <>
            <Checkbox checked={present} />
          </>
        );
      },
    },
    {
      title: t("word.file"),
      dataIndex: "file",
      key: "end",
      render(data: any) {
        console.log({ data });
        if (!data) {
          return "";
        }
        return (
          <div style={{ padding: 3 }}>
           
           <Link target="_blank" to={`${process.env.REACT_APP_API_URL}/${data.path}`}> 
              <Button>{t("word.save")}</Button>
            </Link>
          </div>
        );
      },
    },
    {
      title: t("profile.actions"),
      dataIndex: "id",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/education/${z}`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>{" "}
            <AddSertificate index={z} />
          </Space>
        );
      },
    },
  ],

  languages: [
    {
      title: t("profile.languages"),
      dataIndex: "name",
      key: "id",
      //   width: 300,
    },
    {
      title: t("profile.reading"),
      dataIndex: "reading_level",
      key: "degree_name",
    },
    {
      title: t("profile.speaking"),
      dataIndex: "speaking_level",
      key: "field_of_study",
    },
    {
      title: t("profile.listening"),
      dataIndex: "listening_level",
      key: "field_of_study",
    },
    {
      title: t("profile.writing"),
      dataIndex: "writing_level",
      key: "start",
    },

    {
      title: t("profile.actions"),
      dataIndex: "lang_id",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/languages/${z}`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>
          </Space>
        );
      },
    },
  ],
  work_experience: [
    {
      title: t("profile.organization"),
      dataIndex: "organization",
      key: "organization",
      //   width: 300,
    },
    {
      title: t("profile.position"),
      dataIndex: "position",
      key: "position",
    },
    {
      title: t("profile.job-duties"),
      dataIndex: "job_duties",
      key: "job_duties",
      //   render: (vacancy: VacancyType) => vacancy.uuid,
    },
    {
      title: t("profile.start-date"),
      dataIndex: "start",
      key: "start",
    },
    {
      title: t("profile.end-date"),
      dataIndex: "end",
      key: "end",
    },
    {
      title: t("profile.present"),
      dataIndex: "to_present",
      key: "to_present",
      render: (present: boolean, x: any) => {
        return (
          <>
            <Checkbox checked={present} />
          </>
        );
      },
    },

    {
      title: t("profile.actions"),
      dataIndex: "position",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/work_experience/${z}`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>
          </Space>
        );
      },
    },
  ],
  recommandations: [
    {
      title: t("profile.fullname"),
      dataIndex: "fullname",
      key: "fullname",
    },
    {
      title: t("profile.phone"),
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: t("profile.work-position"),
      dataIndex: "work_position",
      key: "work_position",
    },

    {
      title: t("profile.actions"),
      dataIndex: "position",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/people/${z}?field=recommandations`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>
          </Space>
        );
      },
    },
  ],
  recommender_employees: [
    {
      title: t("profile.fullname"),
      dataIndex: "fullname",
      key: "fullname",
    },
    {
      title: t("profile.phone"),
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: t("profile.work-position"),
      dataIndex: "work_position",
      key: "work_position",
    },

    {
      title: t("profile.actions"),
      dataIndex: "position",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/people/${z}?field=recommender_employees`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>
          </Space>
        );
      },
    },
  ],
  relative_employees: [
    {
      title: t("profile.fullname"),
      dataIndex: "fullname",
      key: "fullname",
    },
    {
      title: t("profile.phone"),
      dataIndex: "phone",
      key: "phone",
    },
    {
      title: t("profile.work-position"),
      dataIndex: "work_position",
      key: "work_position",
    },
    {
      title: t("profile.actions"),
      dataIndex: "position",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/people/${z}?field=relative_employees`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>
          </Space>
        );
      },
    },
  ],
  relatives: [
    {
      title: t("profile.fullname"),
      dataIndex: "fullname",
      key: "fullname",
      render(_: any, item: RelativeType) {
        return (
          <>
            {item.firstname} {item.lastname} {item.fathername}
          </>
        );
      },
    },
    {
      title: t("profile.birthday"),
      dataIndex: "birthday",
      key: "birthday",
    },
    {
      title: t("profile.work_address"),
      dataIndex: "work_adress",
      key: "work_position",
    },
    {
      title: t("profile.address"),
      dataIndex: "adress",
      key: "address",
    },
    {
      title: t("profile.birth_address"),
      dataIndex: "birth_adress",
      key: "birth_address",
    },
    {
      title: t("profile.relative"),
      dataIndex: "relative",
      key: "relative",
    },
    {
      title: t("profile.actions"),
      dataIndex: "position",
      key: "link",
      render(_: any, __: any, z: any) {
        return (
          <Space>
            <Link to={`/profile/edit/relatives/${z}`}>
              <Button>{t("button.edit")}</Button>
            </Link>
            <Button type="primary" onClick={() => deleteByIndex(z)}>
              {t("button.delete")}
            </Button>
          </Space>
        );
      },
    },
  ],
});
