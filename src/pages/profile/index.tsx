import React from "react";
import JobAlert from "./JobAlert";
import { RouteType } from "../../shared/types";
import Overview from "./Overview";
import PersonalInfos from "./PersonalInfos";
import EditProfile from "./EditProfile/Forms/Education";
import Informations from "./EditProfile/Forms/Informations";
import Experience from "./EditProfile/Forms/Education";
import { editRoutes } from "./EditProfile/routes";
import TablePage from "./Table";
import ChangePassword from "./EditProfile/Forms/ChangePassword";
import ChangePhone from "./EditProfile/Forms/ChangePhone";

export const profileRouteConfig: RouteType[] = [
  {
    path: "/profile/job-alert",
    element: <JobAlert />,
  },
  {
    path: "/profile/applications",
    element: <Overview />,
  },
  {
    path: "/profile/personal-infos",
    element: <PersonalInfos />,
  },
  {
    path: "/profile/table",
    element: <TablePage />,
  },
  {
    path: "/profile/change-password",
    element: <ChangePassword />,
  },
  {
    path: "/profile/change-phone",
    element: <ChangePhone />,
  },
  ...editRoutes,
];

