import React, { useState } from "react";
import {
  DatePicker,
  Checkbox,
  Input,
  Form,
  Button,
  Select,
  Space,
  notification,
} from "antd";
import {
  UserDataType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import styles from "../index.module.scss";
import jwtAxios from "../../../../auth/jwt-api";
import { useQuery } from "react-query";
import { Link, useNavigate, useParams } from "react-router-dom";
import Title from "../../../../components/Title";
import dayjs from "dayjs";
import { LoadingOutlined } from "@ant-design/icons";
import { useForm } from "antd/es/form/Form";
import { useLocale } from "../../../../locale/LocaleProvider";

const Experience = () => {
  const { user } = useAuthData();
  const { index } = useParams();
  const navigate = useNavigate();
  const { setUserData } = useAuthAction();
  const work_experience = user?.work_experience?.[Number(index)];
  const [present, setPresent] = useState(!!work_experience?.to_present);
  const [date, setDate] = useState<{ start?: string; end?: string }>({
    start: work_experience?.start,
    end: work_experience?.end,
  });
  const [loading, setLoading] = useState(false);
  const [nextPage, setNextPage] = useState(false);

  const { t } = useLocale();
 
  const onFinish = async (data: any) => {
    if (!(present ? date.start : date.start && date.end)) {
      return notification.error({ message:t("notification.error.date")});
    }
    setLoading(true);
    try {
      data.start = date.start;
      data.end = present ? null : date.end;

      data.to_present = present;

 
      const sendingData = [...(user?.work_experience || [])];

      if (index) {
        sendingData[+index] = data;
      } else {
        sendingData.push(data);
      }

      const res = await jwtAxios.put("/web/profile", {
        work_experience: sendingData,
      });
      setUserData((prev: UserDataType) => ({ ...prev, user: res.data }));
      notification.success({
        message: t(`notification.success.profile-update`),
      });
      nextPage
        ? navigate("/profile/edit/skills")
        : navigate("/profile/table?page=work_experience");
    } catch (error) {
      notification.error({ message: t(`notification.error.profile-update`) });
      console.log(error);
    }
    setLoading(false);
  };
  

  return (
    <>
      <div className="title-button" style={{ margin: "30px 0 100px" }}>
        <Title>{t("profile.work_experience")}</Title>
        <Link to="/profile/table?page=work_experience">
          <Button> {t("button.back")}</Button>
        </Link>
      </div>
      <Form
        name="basic"
        initialValues={{
          organization: work_experience?.organization,
          position: work_experience?.position,
          start: work_experience?.start
            ? dayjs(work_experience.start, "YYYY-MM")
            : null,
          end: work_experience?.end
            ? dayjs(work_experience.end, "YYYY-MM")
            : null,
          job_duties: work_experience?.job_duties,
        }}
        onFinish={onFinish}
        layout="vertical"
        className="profile-image"
      >
        <div className={styles.form}>
          <Form.Item
            label={t("profile.organization")}
            name="organization"
            rules={[
              {
                required: true,
                message: t("input.required.organization"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.position")}
            name="position"
            rules={[
              {
                required: true,
                message: t("input.required.position"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          {present ? (
            <Form.Item
              label={t("profile.start-date")}
              rules={[
                {
                  required: true,
                  message: t("input.required.start-date"),
                },
              ]}
              style={{ gridColumn: "1/2" }}
            >
              <DatePicker
                style={{ width: "100%" }}
                picker="month"
                value={date?.start ? dayjs(date.start, "YYYY-MM") : null}
                onChange={(data: any) => {
                   if (!data) {
                    return setDate({});
                  }

                  setDate({
                    start: `${data.$y}-${(data.$M + 1 + "").padStart(2, "0")}`,
                  });
                }}
              />
            </Form.Item>
          ) : (
            <Form.Item
              label={t("profile.start-date")}
              rules={[
                {
                  required: true,
                  message: t("input.required.start-date"),
                },
              ]}
              style={{ gridColumn: "1/2" }}
            >
              <DatePicker.RangePicker
                style={{ width: "100%" }}
                picker="month"
                value={[
                  date?.start ? dayjs(date.start, "YYYY-MM") : null,
                  date?.end ? dayjs(date.end, "YYYY-MM") : null,
                ]}
                onChange={(data: any) => {
 
                  if (!data?.[0]) {
                    return setDate({});
                  }

                  setDate({
                    start: `${data[0].$y}-${(data[0].$M + 1 + "").padStart(
                      2,
                      "0"
                    )}`,
                    end: `${data[1].$y}-${(data[1].$M + 1 + "").padStart(
                      2,
                      "0"
                    )}`,
                  });
                }}
              />
            </Form.Item>
          )}

          <Checkbox
            onChange={(e) => {
              setPresent(e.target.checked);
            }}
            checked={present}
          >
            {t("profile.present")}
          </Checkbox>
          <Form.Item
            label={t("profile.job-duties")}
            name="job_duties"
            className={styles.textarea}
          >
            <Input.TextArea
              autoSize={true}
              defaultValue={
                index
                  ? undefined
                  : `1. 
2. 
3. 
4. 
5. `
              }
            />
          </Form.Item>
        </div>
        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading}
              icon={loading ? <LoadingOutlined spin /> : null}
              onClick={() => setNextPage(false)}
            >
              {t("button.submit")}
            </Button>
            <Button htmlType="submit" onClick={() => setNextPage(true)}>
              {t("button.submit-continue")}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </>
  );
};

export default Experience;
