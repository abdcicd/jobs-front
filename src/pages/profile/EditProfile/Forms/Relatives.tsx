import { Button, DatePicker, Form, Input, Select, notification } from "antd";
import React, { useState } from "react";
import {
  Link,
  Navigate,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";
import Title from "../../../../components/Title";
import {
  UserActionsType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import styles from "../index.module.scss";
import { PageType, title } from "../../Table";
import jwtAxios from "../../../../auth/jwt-api";
import { LoadingOutlined } from "@ant-design/icons";
import { useLocale } from "../../../../locale/LocaleProvider";
import { useQuery } from "react-query";
import getDate from "../../../../utils/getDate";
import moment from "moment";
import { dateFormat } from "../../../../shared/constants";
import { RelativeType } from "../../../../shared/types";
import dayjs from "dayjs";

function Relatives() {
  const { index } = useParams();
  const { user } = useAuthData();
  const { setUserData } = useAuthAction();
  const navigate = useNavigate();
  const [api, contextHolder] = notification.useNotification();
  const [loading, setLoading] = useState<boolean>(false);
  const field = "relatives";
  const previousUrl = `/profile/table?page=${field}`;
  const [form] = Form.useForm();
  const { t, lang } = useLocale();
  const relative = user?.[field][+(index || 0)];

  const { data: relatives } = useQuery(["relatives",lang], () =>
    jwtAxios.get("/enum/list-relative-degree",{ params: { lang } })
  );

  const onFinish = async (data: any) => {
    if (!form.isFieldsTouched()) {
      notification.error({
        message: t(`notification.error.empty`),
      });
      return;
    }
    data.birthday = data.birthdate.format(dateFormat);
 
    setLoading(true);
    try {
      const userData = [...(user?.[field] || [])];
      if (!index) {
        userData.push(data);
      } else {
        userData[+index] = data;
      }
      const res = await jwtAxios.put("/web/profile", { [field]: userData });

      setUserData((prev: UserActionsType) => ({ ...prev, user: res.data }));
      notification.success({
        message: t(`notification.success.profile-update`),
      });

      navigate(previousUrl);
    } catch (error) {
      notification.error({
        message: t(`notification.error.profile-update`),
      });
      console.dir(error);
    }
    setLoading(false);
  };
  

  return (
    <>
      {contextHolder}
      <div className="title-button" style={{ margin: "30px 0 100px" }}>
        <Title>{t(`profile.${field}`)}</Title>
        <Link to={previousUrl}>
          <Button> {t("button.back")}</Button>
        </Link>
      </div>
      <Form
        name="basic"
        initialValues={
          index
            ? {
                ...relative,
                birthdate: relative?.birthday
                  ? dayjs(relative.birthday, dateFormat)
                  : undefined,
              }
            : {}
        }
        onFinish={onFinish}
        layout="vertical"
        className="profile-image"
        form={form}
      >
        <div className={styles.form}>
          <Form.Item
            label={t("profile.firstname")}
            name="firstname"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.lastname")}
            name="lastname"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.middlename")}
            name="fathername"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.birthday")}
            name="birthdate"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <DatePicker style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            label={t("profile.work_address")}
            name="work_adress"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Input />
          </Form.Item>{" "}
          <Form.Item
            label={t("profile.birth_address")}
            name="birth_adress"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Input />
          </Form.Item>{" "}
          <Form.Item
            label={t("profile.address")}
            name="adress"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Input />
          </Form.Item>{" "}
          <Form.Item
            label={t("profile.relative")}
            name="relative_level"  
            // rules={[
            //   {
            //     required: true,
            //     message: t("input.required.field"),
            //   },
            // ]}
          >
            <Select allowClear
              options={
                relatives?.data?.map(
                  ({ code, name }: { code: string; name: string }) => ({
                    value: code,
                    label: name,
                  })
                ) || []
              }
            />
          </Form.Item>
        </div>
        <Form.Item wrapperCol={{ offset: 20, span: 4 }}>
          <Button
            type="primary"
            htmlType="submit"
            block
            disabled={loading}
            icon={loading ? <LoadingOutlined spin /> : null}
          >
            {t("button.submit")}
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}

export default Relatives;
