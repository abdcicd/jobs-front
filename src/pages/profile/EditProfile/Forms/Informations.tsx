import {
  Button,
  Checkbox,
  DatePicker,
  Form,
  Input,
  InputNumber,
  Select,
  Space,
  Spin,
  notification,
} from "antd";
import styles from "../index.module.scss";
import InputMask from "react-input-mask";
import ProfileImage from "../../../../components/ProfileImage";
import {
  UserDataType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import { useQuery } from "react-query";
import jwtAxios from "../../../../auth/jwt-api";
import { useEffect, useMemo, useState } from "react";
import type { DatePickerProps } from "antd";
import dayjs from "dayjs";
import { useLocale } from "../../../../locale/LocaleProvider";
import { useForm } from "antd/es/form/Form";
import { getPhone } from "../../../../utils/getPhone";
import { ageValidator } from "../../../../utils/validators/ageValidator";
import { dateFormat } from "../../../../shared/constants";
import { useNavigate } from "react-router-dom";
import validateEmail from "../../../../utils/validators/emailValidator";

function Informations() {
  const { user } = useAuthData();
  const { setUserData } = useAuthAction();
  const { t, lang } = useLocale();
  const [form] = useForm();
  const [loading, setLoading] = useState<boolean>(false);
  const [nextPage, setNextPage] = useState(false);
  const [phoneNumber, setPhoneNumber] = useState<string>();

  const navigate = useNavigate();

  const [selectedRegion, setSelectedRegion] = useState(user?.region_id);

  const initialValues = useMemo(
    () => ({
      ...(user || {}),
      date_of_birth: user?.date_of_birth
        ? dayjs(user?.date_of_birth, dateFormat)
        : null,
      has_car: !!user?.has_car,
      gender: user?.gender.type,
      district_id: user?.district_id,
    }),
    []
  );
  const { data: nationalites } = useQuery(["nationalites", lang], () =>
    jwtAxios.get("/web/list-nationalities", { params: { lang } })
  );

  const { data: regions } = useQuery(["regions", lang], () =>
    jwtAxios.get("web/list-regions?type=10", { params: { lang } })
  );

  const { data: genders } = useQuery(["genders", lang], () =>
    jwtAxios.get("/enum/list-gender-types", { params: { lang } })
  );

  const { data: districts } = useQuery(
    ["districts", selectedRegion || "", lang],
    () =>
      jwtAxios.get(
        `web/list-regions?type=20&parent_id=${selectedRegion || ""}`,
        { params: { lang } }
      )
  );

  //Handleing Region change
  const handleRegionChange = (value: any) => {
    setSelectedRegion(value);
  };

  //Handleing Phone change
  const handlePhoneChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setPhoneNumber(e.target.value);
  };

  // useEffect(() => {
  //   if (!districts) {
  //     return;
  //   }

  //   setSelectedDistrict(
  //     user?.region_id === selectedRegion ? user?.district_id : null
  //   );
  // }, [districts]);

  const onFinish = async (data: any) => {
    const contact_phone_2 = getPhone(phoneNumber);

    if (
      !(
        selectedRegion &&
        data.district_id &&
        data.address &&
        data.date_of_birth &&
        data.firstname &&
        data.lastname
      )
    ) {
      return notification.error({
        message: t("notification.error.fill.red"),
      });
    }

    setLoading(true);
    try {
      data.contact_phone_2 = contact_phone_2 && `998${contact_phone_2}`;
      data.has_car = Number(data.has_car);

      data.date_of_birth = data.date_of_birth
        ? `${data.date_of_birth.$y}-${data.date_of_birth.$M + 1}-${
            data.date_of_birth.$D
          }`
        : null;
      const res = await jwtAxios.put("/web/profile", data);
      setUserData((prev: UserDataType) => ({ ...prev, user: res.data }));

      notification.success({
        message: t(`notification.success.profile-update`),
      });

      nextPage && navigate("/profile/edit/education");
    } catch (error: any) {
      notification.error({
        message:
          error.response?.data?.message ||
          t(`notification.error.profile-update`),
      });

      console.dir(error);
    }
    setLoading(false);
  };

  if (loading) {
    return (
      <Spin
        spinning={true}
        style={{
          height: 300,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      ></Spin>
    );
  }

  // if (isNError || isRError) {
  //   return <>ERROR</>;
  // }

  return (
    <Form
      name="basic"
      initialValues={initialValues}
      onFinish={onFinish}
      onFinishFailed={console.log}
      form={form}
      layout="vertical"
      className="profile-image"
    >
      <div className={styles.form}>
        <ProfileImage image={user?.avatar} />
        <Form.Item
          label={t("profile.firstname")}
          name="firstname"
          rules={[{ required: true, message: t("input.required.firstname") }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={t("profile.lastname")}
          name="lastname"
          rules={[{ required: true, message: t("input.required.lastname") }]}
        >
          <Input />
        </Form.Item>
        <Form.Item label={t("profile.middlename")} name="middlename">
          <Input />
        </Form.Item>
        <Form.Item
          label={
            <>
              <span
                style={{
                  color: "rgb(255,0,0,0.5)",
                  fontSize: 20,
                  marginRight: 5,
                }}
              >
                *
              </span>{" "}
              {t("profile.region")}
            </>
          }
          name="region_id"
          rules={[
            {
              validator(_, value) {
                console.log(value);
                if (value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error(t("input.required.field")));
              },
            },
          ]}
        >
          <Select
            onChange={(data) => {
              form.setFieldValue("district_id", "");

              handleRegionChange(data);
            }}
            value={selectedRegion}
            options={regions?.data.data.map(
              (region: { id: number; parent_id: number; name: string }) => ({
                value: region.id,
                label: region.name,
              })
            )}
          />
        </Form.Item>
        <Form.Item
          label={t("profile.date-of-birth")}
          name="date_of_birth"
          rules={[
            { required: true, message: t("input.required.field") },
            { validator: ageValidator },
          ]}
        >
          <DatePicker format={dateFormat} style={{ width: "100%" }} />
        </Form.Item>{" "}
        <Form.Item
          label={t("profile.jshshir")}
          name="jshshir"
          rules={[
            { required: true, message: t("input.required.field") },
            {
              len: 14,
              message: t("input.jshshir.length-14"),
            },
            {
              validator(_, value) {
                if (isNaN(Number(value))) {
                  return Promise.reject(new Error(t("input.error.number")));
                }

                return Promise.resolve();
              },
            },
          ]}
        >
          <Input style={{ width: "100%" }} />
        </Form.Item>{" "}
        <Form.Item
          label={t("profile.gender")}
          name="gender"
          rules={[{ required: true, message: t("input.required.field") }]}
        >
          <Select
            options={genders?.data.map(
              (gender: { code: number; name: string }) => ({
                value: gender.code,
                label: gender.name,
              })
            )}
          ></Select>
        </Form.Item>
        <Form.Item
          label={
            <>
              <span
                style={{
                  color: "rgb(255,0,0,0.5)",
                  fontSize: 20,
                  marginRight: 5,
                }}
              >
                *
              </span>{" "}
              {t("profile.district")}
            </>
          }
          rules={[
            {
              validator(_, value) {
                console.log(value);

                if (value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error(t("input.required.field")));
              },
            },
          ]}
          name={"district_id"}
        >
          <Select
            disabled={typeof selectedRegion !== "number"}
            options={districts?.data.data.map(
              (region: { id: number; parent_id: number; name: string }) => ({
                value: region.id,
                label: region.name,
              })
            )}
          />
        </Form.Item>{" "}
        <Form.Item label={t("profile.contact2")} name="contact_phone_2">
          <InputMask
            mask="+\9\98 (99) 999-99-99"
            placeholder="+998 (99) 999-99-99"
            name="phone"
            id="phone"
            className={"antd-input-style"}
            type="tel"
            value={phoneNumber}
            onChange={handlePhoneChange}
          />
        </Form.Item>{" "}
        <Form.Item
          label={t("profile.nationality")}
          name="nationality_id"
          rules={[{ required: true, message: t("input.required.field") }]}
        >
          <Select
            options={nationalites?.data.data.map(
              (nationality: { id: number; name: string }) => ({
                value: nationality.id,
                label: nationality.name,
              })
            )}
          ></Select>
        </Form.Item>
        <Form.Item
          label={t("profile.address")}
          name="address"
          rules={[{ required: true, message: t("input.required.field") }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label={t("profile.email")}
          name="email"
          rules={[
            () => ({
              validator(_, value) {
                console.log(validateEmail(value));
                if (!value) {
                  return Promise.resolve();
                }
                const error = validateEmail(value);

                if (!error) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error(t(error)));
              },
            }),
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item label={t("profile.place-of-birth")} name="place_of_birth">
          <Input />
        </Form.Item>
        <Form.Item
          label={t("profile.driver-license")}
          name="driver_licence_number"
        >
          <Input style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item label={t("profile.citizenship")} name="citizenship">
          <Input style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item label={t("profile.expected_salary")} name="expected_salary">
          <Input style={{ width: "100%" }} />
        </Form.Item>
        {/* <FormList /> */}
        <Form.Item
          label={t("profile.hobbies")}
          name="hobby"
          className={styles.textarea}
        >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          label={t("profile.strength")}
          name="strengths"
          className={styles.textarea}
        >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          label={t("profile.criminal-responsibility")}
          name="criminal_responsibility"
          className={styles.textarea}
        >
          <Input.TextArea />
        </Form.Item>
        <Form.Item
          label={t("profile.attending-court")}
          name="attending_court"
          className={styles.textarea}
        >
          <Input.TextArea />
        </Form.Item>
        <Form.Item name="has_car" valuePropName="checked">
          <Checkbox>{t("profile.has-car")}</Checkbox>
        </Form.Item>
      </div>
      <Form.Item>
        <Space>
          <Button
            type="primary"
            htmlType="submit"
            onClick={() => setNextPage(false)}
          >
            {t("button.submit")}
          </Button>
          <Button htmlType="submit" onClick={() => setNextPage(true)}>
            {t("button.submit-continue")}
          </Button>
        </Space>
      </Form.Item>
    </Form>
  );
}

export default Informations;
