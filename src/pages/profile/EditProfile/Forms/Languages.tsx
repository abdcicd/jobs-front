import React, { useState } from "react";
import {
  UserDataType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import { Link, useNavigate, useParams } from "react-router-dom";
import Title from "../../../../components/Title";
import { Button, Form, Input, Select, Slider, Space, notification } from "antd";
import styles from "../index.module.scss";
import { LoadingOutlined } from "@ant-design/icons";
import { useQuery } from "react-query";
import jwtAxios from "../../../../auth/jwt-api";
import { useForm } from "antd/es/form/Form";
import { LangType } from "../../../../shared/types";
import { useLocale } from "../../../../locale/LocaleProvider";

function Languages() {
  const { user } = useAuthData();
  const { index } = useParams();
  const navigate = useNavigate();
  const { setUserData } = useAuthAction();
  const { t, lang } = useLocale();

  const [language, setLanguage] = useState(
    user?.languages[Number(index)] || {
      type: null,
      reading_level: 1,
      speaking_level: 1,
      writing_level: 1,
      listening_level: 1,
    }
  );
  const unaviableTypes = user?.languages.map((lang: LangType) => lang.type);
  const [nextPage, setNextPage] = useState(false);
  const [loading, setLoading] = useState(false);
  const [form] = useForm();
  const { data: languages } = useQuery(["languages",lang], () =>
    jwtAxios.get("enum/list-profile-languages",{ params: { lang } })
  );

  const onFinish = async (data: any) => {
    
    data = { ...language, type: data.type };
     setLoading(true);
    try {
      const sendingData = [...(user?.languages || [])];

      if (index) {
        sendingData[+index] = data;
      } else {
        sendingData.push(data);
      }

      const res = await jwtAxios.put("/web/profile", {
        languages: sendingData,
      });

      setUserData((prev: UserDataType) => ({ ...prev, user: res.data }));
      notification.success({ message: t(`notification.success.profile-update`) });
       nextPage ?navigate("/profile/edit/people?field=recommandations") :  navigate("/profile/table?page=languages");
    } catch (error) {
      notification.success({ message: t(`notification.error.profile-update`)  });
      console.log(error);
    }
    setLoading(false);
  };

  return (
    <>
      <div className="title-button" style={{ margin: "30px 0 100px" }}>
        <Title>{t("profile.languages")}</Title>
        <Link to="/profile/table?page=languages">
          <Button> {t("button.back")}</Button>
        </Link>
      </div>
      <Form
        name="basic"
        initialValues={{ ...language }}
        onFinish={onFinish}
        form={form}
        layout="vertical"
        className="profile-image"
      >
        <div className={styles.form}>
          <Form.Item
            label={t("profile.language")}
            name="type"
            rules={[
              {
                required: true,
                message: t("input.required.field"),
              },
            ]}
          >
            <Select
              options={languages?.data
                .filter(
                  (lang: LangType) =>
                    !unaviableTypes?.includes(lang.code) ||
                    lang.code === language.type
                )
                .map((lang: LangType) => ({
                  value: lang.code,
                  label: lang.name,
                }))}
            />
          </Form.Item>
          <Form.Item label={t("profile.reading")} name="reading_level">
            <Slider
              min={1}
              max={5}
              value={language.reading_level}
              onChange={(value) =>
                setLanguage({ ...language, reading_level: value })
              }
            />{" "}
          </Form.Item>
          <Form.Item label={t("profile.speaking")} name="speaking_level">
            <Slider
              min={1}
              max={5}
              value={language.speaking_level}
              onChange={(value) =>
                setLanguage({ ...language, speaking_level: value })
              }
            />{" "}
          </Form.Item>
          <Form.Item label={t("profile.writing")} name="writing_level">
            <Slider
              min={1}
              max={5}
              value={language.writing_level}
              onChange={(value) =>
                setLanguage({ ...language, writing_level: value })
              }
            />{" "}
          </Form.Item>
          <Form.Item label={t("profile.listening")} name="listening_level">
            <Slider
              min={1}
              max={5}
              value={language.listening_level}
              onChange={(value) =>
                setLanguage({ ...language, listening_level: value })
              }
            />{" "}
          </Form.Item>
        </div>
        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading}
              icon={loading ? <LoadingOutlined spin /> : null}
              onClick={() => setNextPage(false)}
            >
              {t("button.submit")}
            </Button>
            <Button htmlType="submit" onClick={() => setNextPage(true)}>
              {t("button.submit-continue")}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </>
  );
}

export default Languages;
