import React, { useState } from "react";
import {
  UserDataType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import { Link, useNavigate, useParams } from "react-router-dom";
import Title from "../../../../components/Title";
import { Button, Form, Input, Select, Slider, Space, notification } from "antd";
import styles from "../index.module.scss";
import { LoadingOutlined } from "@ant-design/icons";
import { useQuery } from "react-query";
import jwtAxios from "../../../../auth/jwt-api";
import { useForm } from "antd/es/form/Form";
import { LangType } from "../../../../shared/types";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";
import { useLocale } from "../../../../locale/LocaleProvider";

function Skills() {
  const { user } = useAuthData();
  const navigate = useNavigate();
  const { setUserData } = useAuthAction();
  const [loading, setLoading] = useState(false);
  const [form] = useForm();  const [nextPage, setNextPage] = useState(false);

  const { t } = useLocale();
  

  const onFinish = async (data: any) => {
    if (!form.isFieldsTouched()) {
      notification.error({
        message: t(`notification.error.empty`),
      });
      return;
    }

    setLoading(true);
    try {
      const res = await jwtAxios.put("/web/profile", data);

      setUserData((prev: UserDataType) => ({ ...prev, user: res.data }));
      notification.success({
        message: t(`notification.success.profile-update`),
      });
      nextPage ? navigate("/profile/edit/languages") : navigate("/profile/edit-profile");
    } catch (error) {
      notification.success({ message: t(`notification.error.profile-update`) });
      console.log(error);
    }
    setLoading(false);
  };

  return (
    <>
      <div className="title-button" style={{ margin: "30px 0 100px" }}>
        <div>
          <Title>{t(`profile.skills`)}</Title>
          <p className="title-page-description">
            {t(`profile.description.template`).replace(
              "{%%}",
              t(`profile.skills`)
            )}
          </p>{" "}
        </div>{" "}
        <Link to="/profile/edit-profile">
          <Button> {t("button.back")}</Button>
        </Link>
      </div>

      <Form
        name="basic"
        initialValues={{ skills: user?.skills }}
        onFinish={onFinish}
        form={form}
        layout="vertical"
        className="profile-image"
      >
        <div className={styles.form}>
          <Form.List name="skills">
            {(fields, { add, remove }, { errors }) => (
              <>
                {fields.map((field, index) => (
                  <Form.Item required={false} key={field.key}>
                    <Form.Item
                      {...field}
                      validateTrigger={["onChange", "onBlur"]}
                      rules={[
                        {
                          required: true,
                          whitespace: true,
                          message: t("input.required.skill"),
                        },
                      ]}
                      noStyle
                    >
                      <Input style={{ width: "calc(100% - 25px)" }} />
                    </Form.Item>
                    <MinusCircleOutlined
                      className="dynamic-delete-button"
                      onClick={() => remove(field.name)}
                      style={{ marginLeft: 10 }}
                    />
                  </Form.Item>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    style={{ width: "60%" }}
                    icon={<PlusOutlined />}
                  >
                    {t("button.add")}
                  </Button>

                  <Form.ErrorList errors={errors} />
                </Form.Item>
              </>
            )}
          </Form.List>
        </div>
        <Form.Item  >
        <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading}
              icon={loading ? <LoadingOutlined spin /> : null}
              onClick={() => setNextPage(false)}  
            >
              {t("button.submit")}
            </Button>
            <Button htmlType="submit" onClick={() => setNextPage(true)}  >
              {t("button.submit-continue")}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </>
  );
}

export default Skills;
