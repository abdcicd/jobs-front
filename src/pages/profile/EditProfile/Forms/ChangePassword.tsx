import React, { useState } from "react";
import styles from "./index.module.scss";
import { Button, Col, Form, Input, Row, Spin, notification } from "antd";
import { useLocale } from "../../../../locale/LocaleProvider";
import jwtAxios from "../../../../auth/jwt-api";
 import { Link } from "react-router-dom";
import Title from "../../../../components/Title";

function ChangePassword() {
  const [form] = Form.useForm();
  const [spinning, setSpinning] = useState(false);
  const { t } = useLocale();
  const onFinish = async (values: any) => {
    setSpinning(true);
    if (values.password === values.old_password) {
      notification.error({
        message: t("notification.error.old-password"),
      });
      setSpinning(false);
 
      return;
    }
    try {
      await jwtAxios.put("/web/user-password-change", values);
      notification.success({ message: t("notification.success.password") });
      form.resetFields();
    } catch (error) {
      notification.error({ message: t("notification.error.wrong") });
    }
    setSpinning(false);
  };

  return (
    <>     
    <div style={{ margin: "30px 0  " }}>
      <Title>{t(`profile.change-password`)}</Title>
      <p className="title-page-description">
        {t(`profile.description.template`).replace(
          "{%%}",
          t(`profile.change-password`)
        )}
      </p>{" "}
    </div>{" "}
     
 
    <Spin spinning={spinning}>
      <Form form={form} onFinish={onFinish}>
        <div className={styles.container}>
          <Row gutter={40}>
            {" "}
            <Col xs={24} md={12}>
              <Form.Item
                name="old_password"
                rules={[{ required: true, message: t("input.error.old-password") } , {
                  min: 8,
                  message:  t("input.password.length-8"),
                }, ]}
              >
                <Input.Password placeholder={t("input.placeholder.password")} />
              </Form.Item>
            </Col>
            <Col xs={24} md={12} />
            <Col xs={24} md={12}>
              <Form.Item
                name="password"
                rules={[
                  { required: true, message: t("input.error.new-password") }, {
                    min: 8,
                    message: t("input.password.length-8"),
                  },
                ]}
              >
                <Input.Password
                  placeholder={t("input.placeholder.new-password")}
                />
              </Form.Item>
            </Col>
 
            <Col xs={24} md={12}>
              <Form.Item
                name="confirmPassword"
                rules={[
                  {
                    required: true,
                    message: t("input.error.new-confirm-password"),
                  }, {
                    min: 8,
                    message:  t("input.password.length-8"),
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue("password") === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(
                        "The Confirm Password that you entered do not match!"
                      );
                    },
                  }),
                ]}
              >
                <Input.Password
                  placeholder={t("input.placeholder.new-confirm-password")}
                />
              </Form.Item>
            </Col>
            <Col xs={24} md={24}>
              <Form.Item shouldUpdate className={styles.buttons}>
                <Button type="primary" htmlType="submit">
                  {" "}
                  {t("button.submit")}
                </Button>
                <Button htmlType="button" onClick={() => form.resetFields()}>
                  {" "}
                  {t("button.cancel")}
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </div>{" "}
      </Form>
    </Spin>    </>

  );
}

export default ChangePassword;
