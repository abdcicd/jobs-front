import React, { useRef, useState } from "react";
import {
  DatePicker,
  Checkbox,
  Input,
  Form,
  Button,
  Select,
  Space,
  notification,
} from "antd";
import {
  UserDataType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import styles from "../index.module.scss";
import jwtAxios from "../../../../auth/jwt-api";
import { useQuery } from "react-query";
import { Link, useNavigate, useParams } from "react-router-dom";
import Title from "../../../../components/Title";
import dayjs from "dayjs";
import { LoadingOutlined } from "@ant-design/icons";
import { useForm } from "antd/es/form/Form";
import { useLocale } from "../../../../locale/LocaleProvider";

const Education = () => {
  const { user } = useAuthData();
  const { index } = useParams();
  const navigate = useNavigate();
  const { setUserData } = useAuthAction();
  const education = user?.education[Number(index)];
  const [date, setDate] = useState<{ start?: string; end?: string }>({
    start: education?.start,
    end: education?.end,
  });
  const [present, setPresent] = useState(!!education?.to_present);
  const [loading, setLoading] = useState(false);
  const [nextPage, setNextPage] = useState(false);
  const { t, lang } = useLocale();
  const fileRef = useRef();

  const { data: degrees } = useQuery(["degrees", lang], () =>
    jwtAxios.get("/enum/list-edu-degree-types", { params: { lang } })
  );

  const onFinish = async (data: any) => {
    if (!(present ? date.start : date.start && date.end)) {
      return notification.error({ message: t("notification.error.date") });
    }
    setLoading(true);
    try {
      data.start = date.start;
      data.end = present ? null : date.end;

      data.to_present = present;

      const sendingData = [...(user?.education || [])];

      if (index) {
        sendingData[+index] = data;
      } else {
        sendingData.push(data);
      }

      const res = await jwtAxios.put("/web/profile", {
        education: sendingData,
      });
      setUserData((prev: UserDataType) => ({ ...prev, user: res.data }));
      notification.success({
        message: t(`notification.success.profile-update`),
      });

      nextPage
        ? navigate("/profile/edit/work_experience")
        : navigate("/profile/table?page=education");
    } catch (error) {
      notification.error({ message: t(`notification.error.profile-update`) });
      console.log(error);
    }
    setLoading(false);
  };
  console.log(date);
  return (
    <>
      <div className="title-button" style={{ margin: "30px 0 100px" }}>
        <Title>{t("profile.education")}</Title>
        <Link to="/profile/table?page=education">
          <Button> {t("button.back")}</Button>
        </Link>
      </div>
      <Form
        name="basic"
        initialValues={{
          school: education?.school,
          field_of_study: education?.field_of_study,
          start: education?.start ? dayjs(education.start, "YYYY-MM") : null,
          end: education?.end ? dayjs(education.end, "YYYY-MM") : null,
          degree: education?.degree,
          activities: education?.activities,
        }}
        onFinish={onFinish}
        layout="vertical"
        className="profile-image"
      >
        <div className={styles.form}>
          {" "}
          <Form.Item
            label={t("profile.degree")}
            name="degree"
            rules={[
              {
                required: true,
                message: t("input.required.degree"),
              },
            ]}
          >
            <Select
              options={degrees?.data?.map(
                (degree: { code: number; name: string }) => ({
                  value: degree.code,
                  label: degree.name,
                })
              )}
            />
          </Form.Item>
          <Form.Item
            label={t("profile.school")}
            name="school"
            rules={[
              {
                required: true,
                message: t("input.required.school"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.field-of-study")}
            name="field_of_study"
            rules={[
              {
                required: true,
                message: t("input.required.field-of-study"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          {present ? (
            <Form.Item
              label={t("profile.start-date")}
              rules={[
                {
                  required: true,
                  message: t("input.required.start-date"),
                },
              ]}
              style={{ gridColumn: "1/2" }}
            >
              <DatePicker
                style={{ width: "100%" }}
                picker="month"
                value={date?.start ? dayjs(date.start, "YYYY-MM") : null}
                onChange={(data: any) => {
                  if (!data) {
                    return setDate({});
                  }

                  setDate({
                    start: `${data.$y}-${(data.$M + 1 + "").padStart(2, "0")}`,
                  });
                }}
              />
            </Form.Item>
          ) : (
            <Form.Item
              label={t("profile.start-date")}
              rules={[
                {
                  required: true,
                  message: t("input.required.start-date"),
                },
              ]}
              style={{ gridColumn: "1/2" }}
            >
              <DatePicker.RangePicker
                style={{ width: "100%" }}
                picker="month"
                value={[
                  date?.start ? dayjs(date.start, "YYYY-MM") : null,
                  date?.end ? dayjs(date.end, "YYYY-MM") : null,
                ]}
                onChange={(data: any) => {
                  if (!data?.[0]) {
                    return setDate({});
                  }

                  setDate({
                    start: `${data[0].$y}-${(data[0].$M + 1 + "").padStart(
                      2,
                      "0"
                    )}`,
                    end: `${data[1].$y}-${(data[1].$M + 1 + "").padStart(
                      2,
                      "0"
                    )}`,
                  });
                }}
              />
            </Form.Item>
          )}
          <Checkbox
            onChange={(e) => {
              setPresent(e.target.checked);
            }}
            checked={present}
          >
            {t("profile.present")}{" "}
          </Checkbox>{" "}
          {/* <Form.Item
            label={t("profile.field-of-study")}
            name=""
            rules={[
              {
                required: true,
                message: t("input.required.field-of-study"),
              },
            ]}
          >
            <input type="file" />
          </Form.Item> */}
          <Form.Item
            label={t("profile.activities")}
            name="activities"
            className={styles.textarea}
          >
            <Input.TextArea autoSize={true} />
          </Form.Item>
        </div>
        <Form.Item>
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading}
              icon={loading ? <LoadingOutlined spin /> : null}
              onClick={() => setNextPage(false)}
            >
              {t("button.submit")}
            </Button>
            <Button htmlType="submit" onClick={() => setNextPage(true)}>
              {t("button.submit-continue")}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </>
  );
};

export default Education;
