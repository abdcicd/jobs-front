import { Button, Form, Input, Space, notification } from "antd";
import React, { useState } from "react";
import {
  Link,
  Navigate,
  useLocation,
  useNavigate,
  useParams,
} from "react-router-dom";
import Title from "../../../../components/Title";
import {
  UserActionsType,
  useAuthAction,
  useAuthData,
} from "../../../../auth/JWTAuthProvider";
import styles from "../index.module.scss";
import { PageType, title } from "../../Table";
import jwtAxios from "../../../../auth/jwt-api";
import { LoadingOutlined } from "@ant-design/icons";
import { useLocale } from "../../../../locale/LocaleProvider";

const nextPageUrls = {
  recommandations: "/profile/edit/people?field=recommender_employees",
  recommender_employees: "/profile/edit/relatives",
};

function People() {
  const { search } = useLocation();
  const { index } = useParams();
  const { user } = useAuthData();
  const { setUserData } = useAuthAction();
  const navigate = useNavigate();
  const [loading, setLoading] = useState<boolean>(false);
  const field = new URLSearchParams(search).get("field") as PageType;
  const previousUrl = `/profile/table?page=${field}`;
  const [nextPage, setNextPage] = useState(false);
  const [form] = Form.useForm();
  const { t } = useLocale();

  const onFinish = async (data: any) => {
    if (!form.isFieldsTouched()) {
      notification.error({
        message: t(`notification.error.empty`),
      });
      return;
    }
    setLoading(true);
    try {
      const userData = [...(user?.[field] || [])];
      if (!index) {
        userData.push(data);
      } else {
        userData[+index] = data;
      }
      const res = await jwtAxios.put("/web/profile", { [field]: userData });

      setUserData((prev: UserActionsType) => ({ ...prev, user: res.data }));
      notification.success({
        message: t(`notification.success.profile-update`),
      });
      form.resetFields()
      nextPage
        ? navigate(
            nextPageUrls[field as "recommandations" | "recommender_employees"]
          )
        : navigate(previousUrl);
    } catch (error) {
      notification.error({
        message: t(`notification.error.profile-update`),
      });
      console.dir(error);
    }
    setLoading(false);
  };
  return (
    <>
      <div className="title-button" style={{ margin: "30px 0 100px" }}>
        <Title>{t(`profile.${field}`)}</Title>
        <Link to={previousUrl}>
          <Button> {t("button.back")}</Button>
        </Link>
      </div>
      <Form
        name="basic"
        initialValues={index ? user?.[field][+index] : {}}
        onFinish={onFinish}
        layout="vertical"
        className="profile-image"
        form={form}
      >
        <div className={styles.form}>
          <Form.Item
            label={t("profile.fullname")}
            name="fullname"
            rules={[
              {
                required: true,
                message: t("input.required.fullname"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.phone")}
            name="phone"
            rules={[
              {
                required: true,
                message: t("input.required.phone"),
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label={t("profile.position")}
            name="work_position"
            rules={[
              {
                required: true,
                message: t("input.required.position2"),
              },
            ]}
          >
            <Input />
          </Form.Item>
        </div>
        <Form.Item  >
          <Space>
            <Button
              type="primary"
              htmlType="submit"
              disabled={loading}
              icon={loading ? <LoadingOutlined spin /> : null}
              onClick={() => setNextPage(false)}  
            >
              {t("button.submit")}
            </Button>
            <Button htmlType="submit" onClick={() => setNextPage(true)}  >
              {t("button.submit-continue")}
            </Button>
          </Space>
        </Form.Item>
      </Form>
    </>
  );
}

export default People;
