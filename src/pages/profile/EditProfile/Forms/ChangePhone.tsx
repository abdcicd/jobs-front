import { Button, Col, Form, Input, Row, notification } from "antd";
import React, { useRef, useState } from "react";
import InputMask from "react-input-mask";
import styles from "./index.module.scss";
import { getPhone } from "../../../../utils/getPhone";
import { useLocale } from "../../../../locale/LocaleProvider";
import { useNavigate } from "react-router-dom";
import jwtAxios from "../../../../auth/jwt-api";

// "notification.error.verify-verification-code": "Biza jonatgan nomerri kriting!",
// "notification.success.verify-verification-code": "Nopmer verify boldi!",
// "notification.error.send-verification-code":"Cannot sent verification code  to {%%}",
// "notification.success.send-verification-code":"verification code sent to {%%}",

function ChangePhone() {
  const [phoneNumber, setPhoneNumber] = useState("");
  const [code, setCode] = useState("");
  const [page, setPage] = useState<"phone" | "code">("phone");
  const { t } = useLocale();
  const navigate = useNavigate();
  const [time, setTime] = useState({ begin: Date.now(), current: Date.now() });
  const intervalRef = useRef<NodeJS.Timer>();
  const resenttime = 60;
  const recentTime = Math.ceil(resenttime - (time.current - time.begin) / 1000);
 console.log(t("notification.success.send-verification-code"));
 
  const timer = () => {
    const beginTime = Date.now();
    setTime({ begin: beginTime, current: beginTime });

    intervalRef.current = setInterval(() => {
      const current = Date.now();
      const recentTime = Math.ceil(resenttime - (current - beginTime) / 1000);

      setTime({ begin: beginTime, current });
      if (recentTime < 1) {
        clearInterval(intervalRef.current);
      }
    }, 1000);
  };
  const sendPhone = async () => {
    const phone = getPhone(phoneNumber);

    if (phone.length !== 9) {
      return notification.error({ message: t("auth.enter-phone") });
    }
    try {
      await jwtAxios.post("/auth/send-verification-code-new-phone", {
        phone: `998${phone}`,
      });
      timer();

      setPage("code");
    } catch (error: any) {
      console.log(error);
      !error?.disabled &&
        notification.error({
          message: t("notification.error.send-verification-code").replace(
            "{%%}",
            phoneNumber
          ),
        });
    }
  };
  const sendCode = async () => {
 if (!code ) {
  return 
 }
    const phone = getPhone(phoneNumber);
 console.log(code);
 
    try {
      await jwtAxios.post("/auth/verify-new-phone-verification-code", {
        code,
        phone: `998${phone}`,
      });
      notification.success({
        message: t("notification.success.verify-verification-code"),
      });

      navigate("/profile/personal-infos");
    } catch (error: any) {
 
      error?.disabled ||
        notification.error({
          message: t("notification.error.verify-verification-code"),
        });
    }
  };
  const resentCode = async () => {
    const phone = getPhone(phoneNumber);

    try {
      await jwtAxios.post("/auth/send-verification-code-new-phone", {
        phone: `998${phone}`,
      });
      timer();
      notification.success({
        message: t("notification.success.send-verification-code").replace(
          "{%%}",
          phone
        ),
      });
    } catch (error: any) {
      !error?.disabled &&
        notification.error({
          message: t("notification.error.send-verification-code").replace(
            "{%%}",
            phone
          ),
        });
      console.log(error);
    }
  };

  return page === "phone" ? (
    <div className={styles.phonepage}>
      <div className={styles["input"]}>
        <p> {t("profile.enter-new-phone")}</p>{" "}
        <InputMask
          mask="+\9\98 (99) 999-99-99"
          placeholder="+998 (99) 999-99-99"
          name="phone"
          id="phone"
          className={"antd-input-style"}
          type="tel"
          value={phoneNumber}
          onChange={(e) => setPhoneNumber(e.currentTarget.value)}
          onKeyDown={(e) => e.key === "Enter" && sendPhone()}
        />
      </div>
      <Button type="primary" onClick={sendPhone}>
        {t("button.send")}
      </Button>
    </div>
  ) : (
    <div>
      <div className={styles.phonepage}>
        <div className={styles["input"]}>
          <p> {t("auth.enter-code")}</p>{" "}
          <Input
            placeholder="Code..."
            onChange={(e) => setCode(e.currentTarget.value)}
            onKeyDown={(e) => e.key === "Enter" && sendCode()}
          />
        </div>
        <Button type="primary" onClick={sendCode}>
          {t("button.send")}
        </Button>
      </div>
      <p className={styles["p"]}>
        <span
          onClick={() => {
            clearInterval(intervalRef.current);
            setPage("phone");
          }}
        >
          {" "}
          {t("word.change-phone-that-you-entered")}: {phoneNumber}
        </span>{" "}
        <p>
          {recentTime < 1 ? (
            <span onClick={resentCode} style={{ cursor: "pointer" }}>
              {t("auth.resend-code")}
            </span>
          ) : (
            t("auth.able-resend").replace("{%%}", recentTime)
          )}
        </p>
      </p>
    </div>
  );
}

export default ChangePhone;
