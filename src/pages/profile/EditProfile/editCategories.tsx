import { AiFillInfoCircle } from "react-icons/ai";
import { BsGraphUpArrow, BsPeopleFill } from "react-icons/bs";
import { IoSchoolSharp } from "react-icons/io5";
import { VscGraph } from "react-icons/vsc";
import { HiTranslate } from "react-icons/hi";
import { FaLock, FaThumbsUp } from "react-icons/fa";
import { MdFamilyRestroom } from "react-icons/md";
import { EditCategoryType } from "../../../shared/types";

export const editCategories: EditCategoryType[] = [
  {
    icon: <AiFillInfoCircle />,
    name: "profile.informations",
    link: "/profile/edit/information",
  },  {
    icon: <HiTranslate />,
    name: "profile.languages",
    link: "/profile/table?page=languages",
  },
 {
    icon: <IoSchoolSharp />,
    name: "profile.education",
    link: "/profile/table?page=education",
  }, {
    icon: <FaThumbsUp />,
    name: "profile.recommandations",
    link: "/profile/table?page=recommandations",
  },

 
  {
    icon: <BsGraphUpArrow />,
    name: "profile.work_experience",
    link: "/profile/table?page=work_experience",
  },

 
  {
    icon: <BsPeopleFill />,
    name: "profile.recommender_employees",
    link: "/profile/table?page=recommender_employees",
  },  {
    icon: <VscGraph />,
    name: "profile.skills",
    link: "/profile/edit/skills",
  },
 
  {
    icon: <MdFamilyRestroom />,
    name: "profile.relatives",
    link: "/profile/table?page=relatives",
  },
];
