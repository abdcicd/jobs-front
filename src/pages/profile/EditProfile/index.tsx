import React from "react";
import Title from "../../../components/Title";
import EditCategories from "../../../components/sections/EditCategories";
import { useLocale } from "../../../locale/LocaleProvider";

function EditProfile() {
  const { t } = useLocale();
 
  return (
    <>
      <Title> {t("profile.edit-profile")}</Title>

      <EditCategories style={{ padding: "70px 0" }} />
    </>
  );
}

export default EditProfile;   