import EditProfile from ".";
import { RouteType } from "../../../shared/types";
import Education from "./Forms/Education";
import Experience from "./Forms/Experience";
import Informations from "./Forms/Informations";
import Languages from "./Forms/Languages";
import People from "./Forms/People";
import Relatives from "./Forms/Relatives";
import Skills from "./Forms/Skills";

export const editRoutes: RouteType[] = [
  {
    path: "/profile/edit-profile",
    element: <EditProfile />,
  },

  {
    path: "/profile/edit/information",
    element: <Informations />,
  },
  {
    path: "/profile/edit/skills",
    element: <Skills />,
  },
  {
    path: "/profile/edit/languages",
    element: <Languages />,
  },
  {
    path: "/profile/edit/languages/:index",
    element: <Languages />,
  },
  {
    path: "/profile/edit/education",
    element: <Education />,
  },
  {
    path: "/profile/edit/education/:index",
    element: <Education />,
  },
  {
    path: "/profile/edit/people",
    element: <People />,
  },
  {
    path: "/profile/edit/people/:index",
    element: <People />,
  },
  {
    path: "/profile/edit/work_experience",
    element: <Experience />,
  },
  {
    path: "/profile/edit/work_experience/:index",
    element: <Experience />,
  },  {
    path: "/profile/edit/relatives",
    element: <Relatives />,
  },
  {
    path: "/profile/edit/relatives/:index",
    element: <Relatives />,
  }, 
];

