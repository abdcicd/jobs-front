 import ActionsTable from "../../../components/sections/ActionsTable";
 import styles from "./index.module.scss"

function Overview() {
  return (
    <div className={styles["overview"]}>
      {/* <div className={styles["top-info-overview"]}>
        <h2>Hello Fazliddin</h2>
        <p>Here are your daily activities & career opportunities</p>{" "}
         
      </div> */}
      <ActionsTable />
    </div>
  );
}

export default Overview;
