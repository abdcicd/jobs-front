 import Profile from "../../../components/sections/Profile";
import UserInfos from "../../../components/sections/UserInfos";

function PersonalInfos() {
  return (
    <div>
      <Profile />
      <UserInfos />
    </div>
  );
}

export default PersonalInfos;
