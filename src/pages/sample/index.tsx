 import { Navigate } from 'react-router-dom';
import { RouteType } from '../../shared/types';
import Companies from './Companies';
import Company from './Company';
import Home from './Home';
import Vacancies from './Vacancies';
import Vacancy from './Vacancy';
import AboutPage from './AboutPage';

export const sampleRouteConfig : RouteType[] = [
  {
    path: '/companies',
    element: <Companies/>,
  },
  {
    path: '/company/:id',
    element: <Company/>,
  },
  {
    path: '/home',
    element: <Home/>,
  },
  {
    path: '/about',
    element: <AboutPage/>,
  },
  {
    path: '/vacancies',
    element: <Vacancies/>,
  },
  {
    path: '/vacancy/:id',
    element: <Vacancy/>,
  },
  {
    path: '/',
    element: <Navigate replace to="/home"/>,
  },
  {
    path: '*',
    element: <Navigate replace to="/not-found"/>,
  },
   
];
