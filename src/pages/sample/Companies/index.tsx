import  { useState } from "react";
import styles from "./company.module.scss";
import CompanyList from "../../../components/sections/CompanyList";
import CompaniesMap from "../../../components/sections/CompaniesMap";
import { CompanyType } from "../../../shared/types";
import BreadCrump from "../../../components/BreadCrump";
import {   useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { Spin } from "antd";
import { useLocale } from "../../../locale/LocaleProvider";
 
function Companies() {
  const [currentCompany, setCurrentCompany] = useState<CompanyType>();  const {t, lang} = useLocale()

  const { data, isLoading,isError } = useQuery(["company",lang], () =>
    jwtAxios.get("/web/list-companies",{ params: { lang } })
  ); 


  if (isLoading) {
    return  <Spin spinning={true} style={{height:300,display:"flex",alignItems:"center",justifyContent:"center"}}></Spin>           
  }  
  
  if (isError) {
    return  <>ERROR</>    

  }

  const companies: CompanyType[] = data?.data.data || [];

  return (
    <>
      <BreadCrump paths={[{label:t("breadcrump.companies"),link:"/company"}  ]} />
      <div className={` ${styles.company} container `}>
        <CompanyList
          setCurrentCompany={setCurrentCompany}
          companies={companies}
        />{" "}
        <CompaniesMap
          currentCompany={currentCompany}
          setCurrentCompany={setCurrentCompany}
          companies={companies}
        />
      </div>
    </>
  );
}

export default Companies;
