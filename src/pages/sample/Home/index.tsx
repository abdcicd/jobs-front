import React from "react";
import Showcase from "../../../components/sections/Showcase";
import Statistics from "../../../components/sections/Statistics";
import About from "../../../components/sections/About";
import TopCompanies from "../../../components/sections/TopCompanies";

function Home() {
  return (
    <>
      <Showcase />
      <Statistics />
      <About />
      <TopCompanies />
    </>
  );
}

export default Home;
