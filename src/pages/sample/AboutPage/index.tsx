import React from "react";
import About from "../../../components/sections/About";
import Statistics from "../../../components/sections/Statistics";

function AboutPage() {
  return (
    <>
      <About />
      <Statistics />
    </>
  );
}

export default AboutPage;
