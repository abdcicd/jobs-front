import { useNavigate, useParams } from "react-router-dom";
import BreadCrump from "../../../components/BreadCrump";
import SliderVacancies from "../../../components/sections/SliderVacancies";
 import CompanyInfo from "../../../components/sections/CompanyInfo";
import Map from "../../../components/sections/Map";
import { useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { CompanyType, VacancyType } from "../../../shared/types";
import { Spin } from "antd";
import { useLocale } from "../../../locale/LocaleProvider";

function Company() {
  const params = useParams();
  const navigate = useNavigate()
const {t, lang} = useLocale()
  const { data, isLoading, isError } = useQuery(["company", params.id,lang], () =>
    jwtAxios.get(`/web/view-company/${params.id}`,{ params: { lang } })
  );  

  if (isLoading) {
    return (
      <Spin
        spinning={true}
        style={{
          height: 300,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      ></Spin>
    );
  }
  if (isError) {
    navigate("/vacancies")
    return <>Error</>;
  }

  const company: CompanyType = data?.data;

  // company.vacancies.forEach((vacancy: VacancyType) => {
  //   vacancy.company = company;
  // });

  return (
    <div  >
      <BreadCrump
        paths={[
          { label: t("word.company"), link: "/companies" },
          { label: company.name, link: "/company/" + company.uuid },
        ]}
      />
      <CompanyInfo company={company} />
    
      {company.vacancies ? (
        <SliderVacancies
          vacancies={company.vacancies}
          title="company.company-vacancies"
        />
      ) : (
        ""
      )}  <Map
        latlng={[company.latitude, company.longitude]}
        style={{ marginBottom: 130 }}
      />
    </div>
  );
}

export default Company;
