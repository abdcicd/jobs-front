 import VacancyInfo from "../../../components/sections/VacancyInfo";
 import {   useNavigate, useParams } from "react-router-dom";
import BreadCrump from "../../../components/BreadCrump";
import SliderVacancies from "../../../components/sections/SliderVacancies";
import { useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { VacancyType } from "../../../shared/types";
import { Spin } from "antd";
import { useAuthData } from "../../../auth/JWTAuthProvider";
import { useLocale } from "../../../locale/LocaleProvider";
 
function Vacancy() {
  const params = useParams();
  const navigate = useNavigate()
  const {user} = useAuthData()
  const {t, lang} = useLocale()
  const queryId = ["vacancy", params.id,lang]
   const { data, isLoading, isError } = useQuery(
    queryId,
    () => jwtAxios.get(`/web/view-vacancy${user ? "-protected" : ""}/${params.id}`,{ params: { lang } })
  );
  const vacancy: VacancyType = data?.data;
 
  if (isLoading) {
    return (
      <>
        <Spin 
          spinning={true}
          style={{
            height: 300,
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        ></Spin>
      </>
    );
  }
  if (isError) {
    navigate("/vacancies")
    return <>Error</>;
  }

  return (
    <>
      <BreadCrump
        paths={[
          { label: t("word.vacancy"), link: "/vacancies" },
          { label: vacancy.name, link: "/vacancy/" + vacancy.uuid },
        ]}
      />
      <VacancyInfo vacancy={vacancy} queryId={queryId}/>
      <SliderVacancies
        vacancies={vacancy.related}
        title={ "vacancy.related-vacancies" }
      />
    </>
  );
}

export default Vacancy;
