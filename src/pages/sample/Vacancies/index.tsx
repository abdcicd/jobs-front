import { useEffect, useState } from "react";
import styles from "./vacancy.module.scss";
import BreadCrump from "../../../components/BreadCrump";
import Filter from "../../../components/sections/Filter";
import VacancyList from "../../../components/sections/VacancyList";
import jwtAxios from "../../../auth/jwt-api";
import { useQuery } from "react-query";
import { FilterType, VacancyType } from "../../../shared/types";
import { Pagination, Spin } from "antd";
import { useLocation } from "react-router-dom";
import { useLocale } from "../../../locale/LocaleProvider";
import getUrl from "../../../utils/getUrl";

function Vacancies() {
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const paramValue = queryParams.get("search");
  const company = queryParams.get("company");
  const { t, lang } = useLocale();

  const [filters, setFilters] = useState<FilterType>({
    page: 1,
    rows: 12,
    s: paramValue || "",
    company,
    lang,
  });
  const api = getUrl(`/web/list-vacancies`, {...filters,lang});
  console.log({ api });

  const { data, isLoading, isError } = useQuery([api ], () => jwtAxios.get(api ));
  const onChange = (page: number) => {
    setFilters((prev) => ({ ...prev, page }));
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, [filters.page]);

  const vacancies: VacancyType[] = data?.data.data || [];

  return (
    <>
      <BreadCrump paths={[{ label: t("word.vacancy"), link: "/vacancies" }]} />
      <div className={styles.vacancy}>
        <div className="container">
          <Filter setFilters={setFilters} filters={filters} company={company} />
          {isLoading ? (
            <Spin
              spinning={true}
              style={{
                height: 300,
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
              }}
            ></Spin>
          ) : isError ? (
            "Error"
          ) : (
            <VacancyList vacancies={vacancies} />
          )}
          <Pagination
            current={filters.page}
            defaultCurrent={1}
            total={data?.data?.meta?.total || 1}
            pageSize={12}
            onChange={onChange}
            style={{ display: "flex", justifyContent: "center" }}
          />
        </div>
      </div>
    </>
  );
}

export default Vacancies;
