import MyButton from "../../../components/Button";
import AuthWrapper from "../AuthWrapper";
import styles from "./signup.module.scss";
import image from "../../../assets/images/logo.png";
import { Link, useNavigate } from "react-router-dom";
import { AiOutlineGoogle } from "react-icons/ai";
import { BiLogoFacebookCircle } from "react-icons/bi";
import { Button, Form, Input } from "antd";
import "./styles.scss";
import { useAuthAction } from "../../../auth/JWTAuthProvider";
import { useLocale } from "../../../locale/LocaleProvider";

function Signup() {
  const { signUpUser } = useAuthAction();
  const {t} = useLocale()
  const navigate = useNavigate()

  const phone = localStorage.getItem("phone")
  const isVerified = localStorage.getItem("isVerified")
if (!phone || !isVerified) {
    navigate("/")
}
  return (
    <AuthWrapper width={600}>
      <div className={`${styles.container} signup-page`}>
        <img src={image} alt="" />

        <Form
          onFinish={x => signUpUser({...x,phone})}
          // initialValues={{
          //   firstname: "Fazliddin",
          //   lastname: "Mirzaqosimov",
          //   username: "victus",
          //   password: "markpolo",
          //   passwordConfirm: "markpolo",
          // }}
        >
          <div className={styles.top}>
            <Form.Item
              name="firstname"
              rules={[
                {
                  required: true,
                  message: t("input.required.firstname"),
                },
              ]}
            >
              <Input placeholder={t("profile.firstname")} />
            </Form.Item>{" "}
            <Form.Item
              name="lastname"
              rules={[
                {
                  required: true,
                  message: t("input.required.lastname"),
                },
              ]}
            >
              <Input placeholder={t("profile.lastname")} />
            </Form.Item>
          </div>
          {/* <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: t("input.required.username"),
              },
            ]}
          >
            <Input placeholder={t("profile.username")} />
          </Form.Item> */}
          <Form.Item
            name="password"
            rules={[
              {
                required: true,
                message: t("input.required.password"),
              },
            ]}
          >
            <Input.Password placeholder={t("auth.password")} />
          </Form.Item>{" "}
          <Form.Item
            name="passwordConfirm"
            rules={[
              {
                required: true,
                message: t("input.required.confirmPassword"),
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("password") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(t("input.required.unmatched"))
                  );
                },
              }),
            ]}
          >
            <Input.Password placeholder={t("auth.passwordConfirm")} />
          </Form.Item>
          {/* <Input placeholder="Confirm password:" /> */}
          <p className={styles.checkbox}>
          {t("auth.yes-acc")} <Link to="/auth/signin"> {t("button.login")}.</Link>
          </p>
          <Button type="primary" htmlType="submit"> {t("button.register")}</Button>
          {/* <div className={styles.socials}>
            <MyButton mode={"outline"} color="grey">
              <AiOutlineGoogle size={20} /> Google
            </MyButton>
            <MyButton mode={"outline"} color="grey">
              <BiLogoFacebookCircle size={20} /> Facebook
            </MyButton>
          </div> */}
        </Form>
      </div>
    </AuthWrapper>
  );
}

export default Signup;
