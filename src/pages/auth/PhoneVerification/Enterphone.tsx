import React, { useState } from "react";
import AuthWrapper from "../AuthWrapper";
import { Button, Form, Input, notification } from "antd";
import { useLocale } from "../../../locale/LocaleProvider";
import styles from "./phone.module.scss";
import InputMask from "react-input-mask";
import jwtAxios from "../../../auth/jwt-api";
import { getPhone } from "../../../utils/getPhone";
import { useNavigate } from "react-router-dom";

function Enterphone() {
  const { t } = useLocale();
  const [phone, setPhone] = useState(
    getPhone(localStorage.getItem("phone") || "", true) || ""
  );
  const navigate = useNavigate();
   const handleClick = async () => {
    try {
      const numberPhone = `998${getPhone(phone)}`;

      if (numberPhone.length !== 12) {
        return;
      }

      await jwtAxios.post("/auth/send-verification-code", {
        phone: numberPhone,
      })  

      localStorage.setItem("phone", numberPhone);
      navigate("/auth/enter-phone-code");
    } catch (error :any ) {
      error?.disabled ||  notification.error({ message:  `Cannot send code to:\n${phone}` });

      console.log(error);
    }
  };
  return (
    <AuthWrapper width={600}>
      <div className={styles.container}>
        {t("auth.enter-phone")}
        <div>
          <InputMask
            mask="+\9\98 (99) 999-99-99"
            placeholder="+998 (99) 999-99-99"
            name="phone"
            id="phone"
            className={ "antd-input-style"}
            type="tel"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            onKeyDown={e=>e.keyCode === 13 && handleClick()}
          />
          <Button type="primary" onClick={handleClick}>
            {t("button.send")}
          </Button>
        </div>
      </div>
    </AuthWrapper>
  );
}

export default Enterphone;
