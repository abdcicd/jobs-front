import AuthWrapper from "../AuthWrapper";
import styles from "./phone.module.scss";
 import { useLocale } from "../../../locale/LocaleProvider";
import { useState, useEffect } from "react";
import { Button, Input, notification } from "antd";
import { Link, useNavigate } from "react-router-dom";
import jwtAxios from "../../../auth/jwt-api";

let x: NodeJS.Timer;

function SendCode() {
  const { t } = useLocale();
  const [code, setCode] = useState("");
  const [time, setTime] = useState({ begin: Date.now(), current: Date.now() });

  const navigate = useNavigate();
  const resenttime = 60;
  const recentTime = Math.ceil(resenttime - (time.current - time.begin) / 1000);


  const timer = () => {
    const beginTime = Date.now()
    setTime({ begin:beginTime, current:beginTime });

    let x = setInterval(() => {
         
      const current = Date.now();
      const recentTime = Math.ceil(resenttime - (current - beginTime) / 1000);

      setTime({ begin:beginTime, current });
      if (recentTime < 1) {
        clearInterval(x);
      }
    }, 1000);
  }

  useEffect(timer, []);

  const handleClick = async () => {
    const numberPhone = localStorage.getItem("phone");

    try {
      await jwtAxios.post("/auth/verify-verification-code", {
        phone: numberPhone,
        code,
      });
      localStorage.setItem("isVerified",'true')
      notification.success({ message: t("notification.success.verify-verification-code")});

      navigate("/auth/signup");
    } catch (error:any) {
      error?.disabled ||  notification.error({ message: t("notification.error.verify-verification-code") });

      console.log(error);
    }
  };

  const resentCode = async () => {
    const numberPhone = localStorage.getItem("phone");
    try {
      if (!numberPhone || numberPhone.length !== 12) {
        return;
      }

      await jwtAxios.post("/auth/send-verification-code", {
        phone: numberPhone,
      });
      timer()
      notification.success({
        message: t("notification.success.send-verification-code").replace("{%%}",numberPhone)  ,
      });
    } catch (error:any) {
   error?.disabled ||   notification.error({ message: t("notification.error.send-verification-code").replace("{%%}",numberPhone) });

      console.log(error);
    }
  };

  // "auth.enter-phone": "Iltimos telefon raqamingizni kiriting!",
  // "auth.enter-code": "Iltimos telefoningizga yuborilgan kodni kiriting!",
  // "auth.code": "Kod",
  // "auth.able-resend": "Kodni {%%} sekunddan so'ng qayta jo'nata olasiz",
  // "auth.change-number": "Raqamni o'zgartirish",
  // "auth.resend-code": "Kodni qayta jo'natish",

  return (
    <AuthWrapper width={600}>
      {" "}
      <div className={styles.container}>
        {t("auth.enter-code")}
        <div>
          <Input
            placeholder={`${t("auth.code")}...`}
            value={code}
            onChange={(e) => setCode(e.target.value)}
          />
          <Button type="primary" onClick={handleClick}>
            {t("button.send")}
          </Button>
        </div>
        <div className={styles.actions}>
          <p>
            {recentTime < 1 ? (
              <span onClick={resentCode} style={{ cursor: "pointer" }}>
                {t("auth.resend-code")}
              </span>
            ) : (
               t("auth.able-resend").replace("{%%}",recentTime)
             )}
          </p>

          <Link to="/auth/enter-phone">{ t("auth.change-number")}</Link>
        </div>
      </div>
    </AuthWrapper>
  );
}

export default SendCode;
