 import Signin from './Signin';
import Signup from './Signup';
import ForgotPassword from './ForgotPassword';
import Enterphone from './PhoneVerification/Enterphone';
import SendCode from './PhoneVerification/SendCode';
 
export const authRouteConfig = [
  {
    path: '/auth/signin',
    element: <Signin/>,
  },
  {
    path: '/auth/signup',
    element: <Signup/>,
  },
  {
    path: '/auth/forget-password',
    element: <ForgotPassword/>,
  },
  {
    path: '/auth/enter-phone',
    element: <Enterphone/>,
  },
  {
    path: '/auth/enter-phone-code',
    element: <SendCode/>,
  },
 
];
