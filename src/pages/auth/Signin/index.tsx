import MyButton from "../../../components/Button";
import { Button } from "antd";
import AuthWrapper from "../AuthWrapper";
import styles from "./signin.module.scss";
import image from "../../../assets/images/logo.png";
import { Link } from "react-router-dom";
import { AiOutlineGoogle } from "react-icons/ai";
import { BiLogoFacebookCircle } from "react-icons/bi";
import { Checkbox, Form, Input } from "antd";
import { useAuthAction, useAuthData } from "../../../auth/JWTAuthProvider";
import { LoadingOutlined } from '@ant-design/icons';
import { useLocale } from "../../../locale/LocaleProvider";
import ReactInputMask from "react-input-mask";
import { useState } from "react";
import { getPhone } from "../../../utils/getPhone";
const x = {
  "auth.remember": "Remember me.",
  "auth.no-acc": "Not have account yet?.",
  "auth.yes-acc": "Already have an account?",
  "auth.confirm": "Confirm password.",
}
function Signin() {
  const [phone, setPhone] = useState("")
  const { signInUser } = useAuthAction();
  const { isLoading } = useAuthData();
  const { t } = useLocale()
  const onFinish = (data: any) => {
    data.phone = `998${getPhone(data.phone)}`

    console.log(data);

    signInUser(data)

  }

  return (
    <AuthWrapper>
      <div className={styles.container}>
        <img src={image} alt="" />

        <Form
          onFinish={onFinish}
          initialValues={{
            rememberMe: true,
            phone:getPhone(localStorage.getItem("phone") || "", true) || ""
            // ,username:"victus",password:"markpolo"
          }
          }
        >

          {/* <Form.Item
            name="username"
            rules={[
              {
                required: true,
                message: t("input.required.username"),
              },
            ]}
          >
            <Input placeholder={t("profile.username")} />
          </Form.Item> */}
          <Form.Item name="phone"
            rules={[
             
              () => ({
                validator(_, value) {
 
                  if (getPhone(value).length === 9) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(t("input.required.phone"))
                  );
                },
              })
            ]} >

            <ReactInputMask
              mask="+\9\98 (99) 999-99-99"
              placeholder="+998 (99) 999-99-99"
              name="phone"
              id="phone"
              className={'antd-input-style'}
              type="tel"
              value={phone}
              onChange={e => setPhone(e.currentTarget.value)}
            />
          </Form.Item>


          <Form.Item
            name="password"
            rules={[() => ({
              validator(_, value) {
                console.log(1111,value);
                
                if (!value) {
                  return Promise.reject(
                    new Error(t("input.required.password"))
                  );                }
                if (value.length >= 8) {
                  return Promise.resolve();
                }
                return Promise.reject(
                  new Error(t("input.password.length-8"))
                );
              },
            })]}
          >
            <Input.Password placeholder={t("auth.password")} />
          </Form.Item>
          <Form.Item
            name="rememberMe"
            valuePropName="checked"
          // wrapperCol={{ offset: 8, span: 16 }}
          >
            <Checkbox>
              {t("auth.remember")}  <Link to="/auth/enter-phone">{t("auth.no-acc")}</Link>
            </Checkbox>
          </Form.Item>
          <Link to="/auth/forget-password">{t("auth.forgot-password")}</Link>
          <br /><br />
          <Form.Item>
            <Button
              style={{ width: "100%" }}
              htmlType="submit"
              type="primary"
              loading={isLoading} icon={isLoading ? <LoadingOutlined spin /> : null}
            >
              {t("button.login")}
            </Button>
          </Form.Item>
          {/* <div className={styles.socials}>
            <MyButton mode={"outline"} color="grey">
              <AiOutlineGoogle size={20} /> Google
            </MyButton>
            <MyButton mode={"outline"} color="grey">
              <BiLogoFacebookCircle size={20} /> Facebook
            </MyButton>
          </div> */}
        </Form>
      </div>
    </AuthWrapper>
  );
}

export default Signin;
