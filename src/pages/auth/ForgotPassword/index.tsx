import Button from "../../../components/Button"
import AuthWrapper from "../AuthWrapper"
import styles from "./repair.module.scss"
import image from "../../../assets/images/logo.png";
import { useState } from "react";
import {   useNavigate } from "react-router-dom";
import { useLocale } from "../../../locale/LocaleProvider";
 import InputMask from "react-input-mask";
import { getPhone } from "../../../utils/getPhone";
import jwtAxios from "../../../auth/jwt-api";
import { notification } from "antd";

function ForgotPassword() {
  const [phone, setPhone] = useState("");
  const { t } = useLocale();
  const navigate = useNavigate();

  const handleClick = async () => {
    const numberPhone = `998${getPhone(phone)}`;
    if (numberPhone.length !== 12) {
      return;
    }
    try {
      await jwtAxios.post("/auth/forget-password", { phone: numberPhone });
      notification.success({
        message: t("notification.success.send-new-password")  ,
      });
      navigate("/auth/signin");
    } catch (error:any) {
    error?.disabled   ||  notification.error({
        message:  t("notification.error.send-new-password"),
      });
    }
  };

  return (
    <AuthWrapper>
      <div className={styles.container}>
        {" "}
        <img src={image} alt="" />
        <p className="open-sans">
          {t("auth.we-will-send-code")}
        </p>
        <span>
          <InputMask
            mask="+\9\98 (99) 999-99-99"
            placeholder="+998 (99) 999-99-99"
            name="phone"
            id="phone"
            className={"antd-input-style"}
            type="tel"
            value={phone}
            onChange={(e) => setPhone(e.target.value)}
            onKeyDown={(e) => e.keyCode === 13 && handleClick()}
          />
          <Button type="button" onClick={handleClick}>
            {t("button.send")}
          </Button>
        </span>
      </div>
    </AuthWrapper>
  );
}

export default ForgotPassword