import {
  AiFillLinkedin,
  AiFillYoutube,
  AiOutlineInstagram,
  AiOutlinePhone,
} from "react-icons/ai";
import { BiLinkAlt, BiLogoFacebookCircle } from "react-icons/bi";
import { FaTelegram } from "react-icons/fa";

function getLinkIcon(
  icon?:
    | "youtube"
    | "instagram"
    | "link"
    | "telegram"
    | "linkedin"
    | "facebook"
    | "phone"
) {
  switch (icon) {
    case "link":
      return <BiLinkAlt />;
    case "phone":
      return <AiOutlinePhone />;

    case "instagram":
      return <AiOutlineInstagram />;

    case "telegram":
      return <FaTelegram />;

    case "youtube":
      return <AiFillYoutube />;

    case "linkedin":
      return <AiFillLinkedin />;

    case "facebook":
      return <BiLogoFacebookCircle />;

    default:
      return <BiLinkAlt />;
  }
}

export default getLinkIcon;
