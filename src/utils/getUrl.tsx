export default function getUrl(path: string, filter: any) {
    const query = [`${path}?`];
  
    for (const [key, value] of Object.entries(filter)) {
      value &&  query.push(`${key}=${value}`);
    }
  
    return query.join("&").replace("&",'');
  }
  
