export const download = async (
  title: string,
  {
    binarydata,
    path,
  }: { binarydata?: Blob; path: string } | { binarydata: Blob; path?: string }
) => {
  let dataUrl = path;

  if (!dataUrl && binarydata) {
    const blob = new Blob([binarydata]);
    dataUrl = window.URL.createObjectURL(blob);
  }

  const downloadLink = document.createElement("a");
  downloadLink.href = dataUrl || "";
  downloadLink.download = title;
  document.body.appendChild(downloadLink);

  downloadLink.click();

  document.body.removeChild(downloadLink);
};
