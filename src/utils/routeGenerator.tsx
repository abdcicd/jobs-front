import { RouteType } from "../shared/types";
import { Routes, Route } from "react-router-dom";

function useRouteGenerator(routes: RouteType[]) {
  return (
    <Routes>
      {routes.map((route: RouteType) => (
        <Route {...route}></Route>
      ))}
    </Routes>
  );
}

export default useRouteGenerator;
