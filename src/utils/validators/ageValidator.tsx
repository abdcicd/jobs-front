import moment from "moment";


function getYearDiff(dateOne: moment.Moment, dateTwo: moment.Moment) {
  return -dateTwo.diff(dateOne, "years");
}


export const ageValidator = (_: any, birthDate: moment.Moment) => {
  if (!birthDate) {
    return Promise.resolve()
  }
  const currentDate = moment();

  // Function call
  let result = getYearDiff(currentDate, birthDate);

  if (result < 18) {
    return Promise.reject("Yoshingiz 18 dan katta bo'lishi kerak");
  }

  return Promise.resolve();
};
