export const getPhone = (phone: string ="", toMask: boolean = false) => {
  if (!phone) {
      return ""
    }
   
   if (toMask) {
  
    return (
      phone &&
      `+998 (${phone.substring(3, 5)}) ${phone.substring(
        5,
        8
      )}-${phone.substring(8, 10)}-${phone.substring(10, 12)}`
    );
  }
  
  const regex = /[_\-\(\)\+\s]/g;

   return phone.substring(4).replace(regex, "");
};
