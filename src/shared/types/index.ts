import React from "react";

export type MainPropType = {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
};

export type RouteType = {
  path: string;
  element: JSX.Element;
};

export type RouteStructureType = {
  fallbackPath: string;
  routes: RouteType[];
};

export type CompanyType = {
  logo?: string;
  name: string;
  description: string;
  longitude: number;
  latitude: number;
  status: number;
  id: number;
  region?: string;
  district?: string;

  uuid: string;
  links: LinkType[];
  vacancies: VacancyType[];

  phone?: (number | string)[];
};

export type VacancyType = {
  count?: number;
  id: number;
  uuid: string;
  name: string;
  applied: boolean;
  company_id: string;
  company: CompanyType;
  description: string;
  requirements: string;
  conditions: string;
  start_date: string;
  end_date: string;
  working_type: string;
  job_type: string;
  salary: string;
  schedule: string;
  related: VacancyType[];
  status: number;
  tags: string[];
  tasks: string;
};

export type RecentActionType = {
  id: number;
  vacancy: VacancyType;
  date: number;
  status: string;
};

export type LinkType = {
  order?: number;
  url: string;
  icon: "youtube" | "instagram" | "link" | "telegram" | "linkedin" | "phone";
};

export type FilterType = {
  s: string;
  page: number;
  rows: number;
  [key: string]: any;
};

export type EditCategoryType = {
  icon: JSX.Element;
  name: string;
  link: string;
};
export type LangType = {
  type: number;
  code: number;
  name: string;
  reading_level: number;
  speaking_level: number;
  writing_level: number;
  listening_level: number;
};
export type RelativeType = {
  firstname: string;
  lastname: string;
  fathername: string;
  birthday: string;
  birthdate: string;
  work_address: string;
  birth_address: string;
  address: string;
  relative_level: number;
  relative?: string;
};

export type UserType = {
  id: number;
  jshshir?: string | null;
  user_id: number;
  username: string;
  firstname: string;
  avatar: string;
  lastname: string;
  middlename: string;
  expected_salary?: string;
  date_of_birth: string;
  nationality_id: number;
  region_id: number;
  district_id: number;
  nationality_name: string;
  region_name: string;
  district_name: string;
  address: string;
  place_of_birth: string;
  citizenship: string;
  contact_phone: string;
  contact_phone_2: string;
  email: string;
  driver_licence_number: string;
  has_car: number;
  hobby: string;
  strengths: string;
  criminal_responsibility: string;
  attending_court: number;
  skills: string[];
  gender: { name: string; code: number; type: number };
  work_experience: {
    organization: string;
    position: string;
    job_duties: string;
    start: string;
    to_present: boolean;
    end: string;
  }[];
  relatives: RelativeType[];
  education: {
    school: string;
    degree: number;
    field_of_study: string;
    start: string;
    to_present: boolean;
    end: string;
    activities: string;
    file: { path: string; id: number };
  }[];
  languages: LangType[];
  recommandations: {
    fullname: string;
    work_position: string;
    phone: number;
  }[];
  relative_employees: {
    fullname: string;
    work_position: string;
    phone: number;
  }[];
  recommender_employees: {
    fullname: string;
    work_position: string;
    phone: number;
  }[];
};
