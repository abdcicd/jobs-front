


 
export const   titles = {
"/home": "page.title.home"
}

export const API_URL = process.env.REACT_APP_API_URL

export const MONTHS = {
    uz : [
        "Yanvar",
        "Fevral",
        "Mart",
        "Aprel",
        "May",
        "Iyun",
        "Iyul", 
        "Avgust",
        "Sentyabr", 
        "Oktyabr",
        "Noyabr",
        "Dekabr",
    ]
}

export const dateFormat = "YYYY-MM-DD"