import { createContext, useContext, useEffect, useState } from "react";
import jwtAxios, { setAuthToken } from "./jwt-api";
import { MainPropType, UserType } from "../shared/types";
import { useNavigate } from "react-router-dom";
import { notification } from "antd";
import { useLocale } from "../locale/LocaleProvider";

export type UserDataType = {
  user?: UserType | null;
  isAuthenticated: boolean;
  isLoading: boolean;
};

export type UserActionsType = {
  signUpUser: (x: {
    name: string;
    email: string;
    password: string;
  }) => Promise<void>;
  signInUser: (x: { email: string; password: string }) => Promise<void>;
  logout: () => Promise<void>;
};

const JWTAuthContext = createContext<UserDataType>({
  user: null,
  isAuthenticated: false,
  isLoading: true,
});

const JWTAuthActionsContext = createContext<any>({});

export const useAuthData = () => useContext(JWTAuthContext);
export const useAuthAction = () => useContext(JWTAuthActionsContext);

const JWTAuthAuthProvider = ({ children }: MainPropType) => {
  const [userData, setUserData] = useState<{
    user: UserType | null;
    isAuthenticated: boolean;
    isLoading: boolean;
  }>({
    user: null,
    isAuthenticated: false,
    isLoading: true,
  });
   
  const navigate = useNavigate();
  const [api, contextHolder] = notification.useNotification();
  const { t, lang } = useLocale();

  useEffect(() => {
    const getAuthUser = () => {
      const token = localStorage.getItem("token");

      if (!token) {
        setUserData({
          user: null,
          isLoading: false,
          isAuthenticated: false,
        });
        return;
      }
      setAuthToken(token);
      jwtAxios
        .get("/web/profile", {
          params: { lang: localStorage.getItem("language") },
        })
        .then(({ data }) => {
          setUserData({
            user: data,
            isLoading: false,
            isAuthenticated: true,
          });
        })
        .catch(() =>
          setUserData({
            user: null,
            isLoading: false,
            isAuthenticated: false,
          })
        );
    };

    getAuthUser();
  }, []);

  const signInUser = async ({
    phone,
    password,
    rememberMe,
  }: {
    phone: string;
    password: string;
    rememberMe: boolean;
  }) => {
    try {
      setUserData((prev) => ({ ...prev, isLoading: true }));

      const { data } = await jwtAxios.post("auth/login", {
        grant_type: "password",
        client_secret: "Mt57LfRyUwwWIuKfSXnNzQAeWxQY0JFNerkrLymd",
        client_id: 2,
        username: phone,
        password,
      });

      setAuthToken(data.access_token);
      const res = await jwtAxios.get("/web/profile", { params: { lang } });

      setUserData({ user: res.data, isAuthenticated: true, isLoading: false });

      rememberMe || localStorage.removeItem("token");
      navigate("/profile/personal-infos");
      // api.success({
      //   message: t("notification.success.login"),
      // });
    } catch (error: any) {
      setUserData({
        ...userData,
        isAuthenticated: false,
        isLoading: false,
      });
    }
  };

  const signUpUser = async ({
    firstname,
    lastname,
    password,
    phone,
  }: {
    firstname: string;
    lastname: string;
    password: string;
    phone: string;
  }) => {
    try {
      await jwtAxios.post("/auth/register", {
        firstname,
        lastname,
        password,
        phone,
      });
      api.success({ message: t("notification.success.register") });
      navigate("/auth/signin");
    } catch (error: any) {
      api.error({
        message:
          error.response.data.errors.username?.[0] ||
          error.response.data.errors.password?.[0] ||
          t("notification.error.register"),
      });

      setUserData({
        ...userData,
        isAuthenticated: false,
        isLoading: false,
      });
    }
  };

  const logout = async () => {
    setAuthToken();
    setUserData({
      user: null,
      isLoading: false,
      isAuthenticated: false,
    });
    // api.success({
    //   message: t("notification.success.logout"),
    // });
  };

  const refetchProfile = async () => {
    try {
      const res = await jwtAxios.get("/web/profile", {
        params: { lang: localStorage.getItem("language") },
      });
      setUserData({ user: res.data, isAuthenticated: true, isLoading: false });
      return res;
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {contextHolder}
      <JWTAuthContext.Provider
        value={{
          ...userData,
        }}
      >
        <JWTAuthActionsContext.Provider
          value={{
            signUpUser,
            signInUser,
            logout,
            setUserData,
            refetchProfile,
          }}
        >
          {children}
        </JWTAuthActionsContext.Provider>
      </JWTAuthContext.Provider>
    </>
  );
};
export default JWTAuthAuthProvider;
