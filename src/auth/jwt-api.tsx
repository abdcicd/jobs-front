import axios from "axios";
import { API_URL } from "../shared/constants";
import { notification } from "antd";
import { LangType, useLocale } from "../locale/LocaleProvider";
import { t } from "../locale/languages";

const jwtAxios = axios.create({
  baseURL: `${API_URL}/api`, //YOUR_API_URL HERE
  headers: {
    "Content-Type": "application/json",
  } 
});
jwtAxios.interceptors.response.use(
  (res) => res,
  (err) => {
    
    if (err.response && err.response.data?.message === "Unauthenticated.") {
      localStorage.removeItem("token");
      window.location.replace("/auth/signin");
    }

    console.log(
      err?.response?.data?.message,
      t(`api.${err?.response?.data?.message?.trim()}`)
    );

    if (t(`api.${err?.response?.data?.message?.trim()}`)) {
      notification.error({
        message: t(`api.${err?.response?.data?.message?.trim()}`),
      });
      return Promise.reject({ disabled: true });
    }

    return Promise.reject(err);
  }
);

export const setAuthToken = (token?: string) => {
  if (token) {
    jwtAxios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    localStorage.setItem("token", token);
  } else {
    delete jwtAxios.defaults.headers.common["Authorization"];
    localStorage.removeItem("token");
  }
};

 
export default jwtAxios;
