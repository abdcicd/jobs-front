import React from "react";
import styles from "./title.module.scss";

function Title({
  className,
  size = "middle",
  ...rest
}: { size?: "small" | "middle" | "big" } & React.DetailedHTMLProps<
  React.HTMLAttributes<HTMLHeadingElement>,
  HTMLHeadingElement
>) {
  return (
    <h1
      {...rest}
      className={`${className} ${styles.title} ${styles[size]} montserrat`}
    ></h1>
  );
}

export default Title;
