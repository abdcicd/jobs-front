import { Button, Checkbox, DatePicker, Form, Input, Select } from "antd";
import React, { useState } from "react";
import { AiOutlineMinusCircle } from "react-icons/ai";
import styles from "../../pages/profile/EditProfile/index.module.scss";

function EducationForm({
  degrees,
  fields,
  remove,
  field,
  index,...rest
}: {
  degrees: any;
  fields: any;
  remove: any;
  field: any;
  index: number;
}) {
  const [present, setPresent] = useState(false);
  return (
  
    <>
 
      <div className={styles.form}>
        <Form.Item
          
          label="School"
          name="school"
          rules={[
            {
              required: true,
              message: "Please input your school!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Field of study"
          name="field_of_study"
          rules={[
            {
              required: true,
              message: "Please input your field!",
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item label="Degree" name="degree" >
          <Select
            defaultValue={1}
            options={degrees?.data?.map(
              (degree: { code: number; name: string }) => ({
                value: degree.code,
                label: degree.name,
              })
            )}
          />
        </Form.Item>
        <Form.Item
          label="Start date"
          name="start"
          
          rules={[
            {
              required: true,
              message: "Please input your start date!",
            },
          ]}
        >
          <DatePicker />
        </Form.Item>
        <Form.Item label="End date" name="end" >
          <DatePicker 
          disabled={present} 
          />
        </Form.Item>
        <Form.Item name="to_present" >
          <Checkbox
            onChange={(e) => {
              setPresent(e.target.checked);
            }}
          >
            Is present
          </Checkbox>
        </Form.Item>
        <Form.Item
          label="Activities"
          name="activities"
          className={styles.textarea}
          
        >
          <Input.TextArea />
        </Form.Item>{" "}
      </div>
   
  </> 
  );
}

export default EducationForm;
