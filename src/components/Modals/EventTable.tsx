import { Table } from "antd";
import React from "react";
import { MONTHS } from "../../shared/constants";
import getDate from "../../utils/getDate";

function EventTable({ events, t }: { events: any; t: any }) {
  const columns = [
    {
      title: t("profile.status"),
      dataIndex: "status",
      key: "status",
      render(_: any, data: any) {
        return (
          <>
            {data.old_status} {"-->"} {data.status}
          </>
        );
      },
    },
    {
      title: t("profile.comment"),
      dataIndex: "comment",
      key: "comment",
      // render(date: string) {
      //   return (
      //     <>
      //       {date.substr(0, 10)} {date.substr(11, 5)}{" "}
      //     </>
      //   );
      // },
    },
    {
      title: t("profile.changed-at"),
      dataIndex: "created_at",
      key: "created_at",
      render:getDate
    },
  ];

  return <Table pagination={false}  dataSource={events || []} columns={columns} />;
}

export default EventTable;
