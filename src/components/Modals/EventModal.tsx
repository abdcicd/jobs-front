import React, { useState } from "react";
import { MainPropType } from "../../shared/types";
import { Button, Modal } from "antd";
import EventTable from "./EventTable";
 import { useLocale } from "../../locale/LocaleProvider";

function EventModal({ children, events }: { events: any } & MainPropType) {
  const [visible, setVisibile] = useState(false);
 const {t} = useLocale()
  return (
    <>
      <Modal
        visible={visible}
        onCancel={() => setVisibile(false)}
        footer={[<Button onClick={() => setVisibile(false)}>{t("button.back")}</Button>]}
        width={1000} title={t("profile.events")}
      >
        <div
          style={{
            margin: "50px 0 0",
             maxHeight: "60vh",
            overflow: "auto",
          }}
        >
           <EventTable t={t} events={events} />
        </div>{" "}
      </Modal>

      <span onClick={() => setVisibile(true)}>{children}</span>
    </>
  );
}

export default EventModal;
