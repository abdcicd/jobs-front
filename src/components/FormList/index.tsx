import { Input, Form, Button } from "antd";
import React from "react";
import { AiOutlineMinusCircle, AiOutlinePlusCircle } from "react-icons/ai";

function FormList() {
  return (
    <Form.List name="phone" initialValue={[""]}>
      {(fields, { add, remove }, { errors }) => (
        <>
          {fields.map((field, index) => (
            <Form.Item
              label={index + 1 === 2 ? "Addicional number" :`Phone` }
              required={false}
              key={field.key}
            >
              <Form.Item
                {...field}
                validateTrigger={["onChange", "onBlur"]}
              
                noStyle
              >
                <Input
                  placeholder="passenger name"
                  style={{ width: "calc(100% - 25px)", marginRight: 5 }}
                />
              </Form.Item>
              {fields.length > 1 ? (
                <AiOutlineMinusCircle
                  className="dynamic-delete-button"
                  onClick={() => remove(field.name)}
                />
              ) : null}
            </Form.Item>
          ))}
          {fields.length < 2 && (
            <Form.Item>
              <Button
                type="dashed"
                onClick={() => add()}
                style={{ width: "100%" }}
                icon={<AiOutlinePlusCircle />}
              >
                Add Phone
              </Button>

              <Form.ErrorList errors={errors} />
            </Form.Item>
          )}
        </>
      )}
    </Form.List>
  );
}

export default FormList;
