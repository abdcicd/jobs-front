import React from "react";
import styles from "./crump.module.scss";
import { Link, useLocation } from "react-router-dom";
import { useLocale } from "../../locale/LocaleProvider";
 
function BreadCrump({ paths }: { paths: { label: string; link: string }[] }) {
  const {t} = useLocale()
  const location = useLocation()

   if (location.pathname.startsWith("/profile")) {
    return (
      <div className={styles.breadCrump}>
        <div  >
          <Link to="/home">{t("breadcrump.home")}</Link>
  
          {paths.map((path: { label: string; link: string }) => (
            <>
              <p>/</p>
              <Link to={path.link}>{path.label}</Link>
            </>
          ))}
        </div>
      </div>)
  }
  return (
    <div className={styles.breadCrump} style={{padding: '0 5%'  }}>
      <div className="container">
        <Link to="/home">{t("breadcrump.home")}</Link>

        {paths.map((path: { label: string; link: string }) => (
          <>
            <p>/</p>
            <Link to={path.link}>{path.label}</Link>
          </>
        ))}
      </div>
    </div>
  );
}

export default BreadCrump;
