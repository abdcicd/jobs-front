import React from "react";
import { MainPropType } from "../../shared/types";
import styles from "./index.module.scss";

function FieldSet({
  style,
  children,
  title,
  entities,
  double=false
}: {
  double?: boolean;
  title: string;
  entities?: [string | number, string | number][];
} & MainPropType) {
  return (
    <div className={styles.container} style={style}>
      <h1>
        <span>{title}</span>
      </h1>

      {children || (
        <div
          className={`${styles.body} ${double && styles.double}`}
         
        >
          {entities?.map((entity) => (
            <p className={styles.field}  >
              <span>{entity[0]}</span>
              {entity[1]}
            </p>
          ))}
        </div>
      )}
    </div>
  );
}

export default FieldSet;
