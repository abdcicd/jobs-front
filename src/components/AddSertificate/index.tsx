import React, { useState, useRef } from "react";
import { useAuthAction, useAuthData } from "../../auth/JWTAuthProvider";
import { Button, notification } from "antd";
import { useLocale } from "../../locale/LocaleProvider";
import jwtAxios from "../../auth/jwt-api";

export default function AddSertificate({ index }: { index: number }) {
  const { user } = useAuthData();
  const { setUserData } = useAuthAction();
  const { lang, t } = useLocale();
  const [loading, setLoading] = useState(false);
  const inputRef = useRef<HTMLInputElement>(null);
  const [api, contextHolder] = notification.useNotification();
  if (!user) {
    return <> </>;
  }
  const sendFile = async (files: FileList | null) => {
    const file = files?.[0];
    console.log(file);

    if (!file) {
      return;
    }
    if (file.size > 2 * 1000000) {
      console.log(1111);

      api.error({
        message: t("notification.error.2mb"),
      });
      return;
    }

    setLoading(true);
    try {
      const education = user.education[index];
      const oldFileId = user.education[index]?.file?.id;
      const formData = new FormData();
      formData.append("file", file);

      const res = await jwtAxios.post(
        `/web/profile-upload-doc?lang=${lang}`,
        formData,
        {
          headers: { "Content-Type": "multipart/form-data" },
        }
      );
      education.file = {
        path: res.data.path,
        id: res.data.id,
      };
      const newUser = await jwtAxios.put("/web/profile", {
        education: user.education,
      });
      // jpg, jpeg, png, gif, pdf, doc, docx
      newUser?.data &&
        setUserData((prev: any) => ({ ...prev, user: newUser.data }));

      notification.success({
        message: t("notification.success.file"),
      });

      oldFileId &&
        (await jwtAxios.delete(`/web/profile-delete-doc/${oldFileId}`));
    } catch (error: any) {
      notification.error({
        message: error?.response?.data?.message,
      });
    }
    setLoading(false);
  };

  return (
    <>
      <abbr title={t("notification.error.2mb")}>
        <Button loading={loading} onClick={() => inputRef.current?.click()}>
          {t("button.file-upload")}
        </Button>
      </abbr>
      <input
        ref={inputRef}
        accept=".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.gpg"
        type="file"
        id="inputfile"
        style={{
          display: "none",
        }}
        onChange={(e) => {
          sendFile(e.target.files);
        }}
      />
      {contextHolder}
    </>
  );
}
