import styles from "./input.module.scss"
import React from 'react'

function Input({className,...rest} :  React.InputHTMLAttributes<HTMLInputElement>) {
  return (
<input  className={`${className} ${styles.input}`}  {...rest}  />  )
}

export default Input