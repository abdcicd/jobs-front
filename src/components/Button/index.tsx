import React from "react";
import styles from "./button.module.scss";

function Button({
  mode,
  color,
  className,
  ...rest
}: {
  mode?: "outline";
  color?: "grey" | "pink";
} & React.ButtonHTMLAttributes<HTMLButtonElement>) {
  return (
    <button
      {...rest}
      className={`${className} ${styles.button}   ${styles[`${mode}`]} ${
        styles[`${color}`]
      } roboto`}
    ></button>
  );
}

export default Button;
