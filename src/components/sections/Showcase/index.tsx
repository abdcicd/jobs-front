import React, { useState } from "react";
import styles from "./hero.module.scss";
import Title from "../../Title";
import image from "../../../assets/images/hero.png";
import { BiSearch } from "react-icons/bi";
import { useNavigate } from "react-router-dom";
import { useLocale } from "../../../locale/LocaleProvider";

function Showcase() {
  const [text, setText] = useState("");
  const navigate = useNavigate();
  const { t } = useLocale();



  return (
    <div className={styles.hero}>
      <div className={styles.text}>
        <Title size="big">
          <span>{t("home.showcase.title.span")}</span> {t("home.showcase.title")}
        </Title>
        <p>{t("home.showcase.paragraph")}</p>
        <form
          action=""
          onSubmit={(e) => {
            e.preventDefault();
            navigate(`/vacancies?search=${text}`);
          }}
        >
          <button type="submit">
            <BiSearch />
          </button>
          <input
            type="text"
            placeholder={t("home.showcase.placeholder")+"..."}
            onChange={(e) => setText(e.currentTarget.value)}
          />
        </form>
      </div>
      <div className={styles.image}>
        <img src={image} alt="" />
      </div>
    </div>
  );
}

export default Showcase;
