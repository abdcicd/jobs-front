import React from "react";
import styles from "./about.module.scss";
import Title from "../../Title";
import image from "../../../assets/images/admm.png";
import { useLocale } from "../../../locale/LocaleProvider";

function About() {
  const { t } = useLocale();

  return (
    <div className={styles.about} id="about">
      <div className="container">
        <div className={styles.text}>
          <Title>
            <span> {t("about.title.span")} </span> {t("about.title")}
          </Title>
          <h2>{t("about.small-title")}</h2>
          <p>{t("about.paragraph")}</p>
        </div>
        <img src={image} alt="" />
      </div>
    </div>
  );
}

export default About;
