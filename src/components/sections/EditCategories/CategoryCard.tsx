import React from "react";
import { EditCategoryType } from "../../../shared/types";
import { Link } from "react-router-dom";
import styles from "./index.module.scss"

function CategoryCard({ category ,t}: {t:any, category: EditCategoryType }) {
  
  return (
    <Link className={styles.card} to={category.link}>
      {category.icon} {t(category.name)}
    </Link>
  );
}

export default CategoryCard;
