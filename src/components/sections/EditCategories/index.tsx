import React from "react";
import { editCategories } from "../../../pages/profile/EditProfile/editCategories";
import { Link } from "react-router-dom";
import styles from "./index.module.scss";
import CategoryCard from "./CategoryCard";
import { MainPropType } from "../../../shared/types";
import { useLocale } from "../../../locale/LocaleProvider";

function EditCategories({ style }: MainPropType) {
  const {t} = useLocale()
  return (
    <div className={styles.cards} style={style}>
      {editCategories.map((category) => (
        <CategoryCard category={category} t={t}/>
      ))}
    </div>
  );
}

export default EditCategories;
 