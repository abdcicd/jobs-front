import { useLocale } from "../../../locale/LocaleProvider";
import { VacancyType } from "../../../shared/types";
import styles from "./vacancy.module.scss";
import VacancyCard from "./VacancyCard";
 
function VacancyList({
  vacancies,
 }: {
  vacancies: VacancyType[];
 }) {
const {t} = useLocale()
   
  return (
    <>
    {vacancies.length ?
      <div className={styles["vacancy-list"]}>
        {vacancies.map((vacancy: VacancyType) => (
          <VacancyCard vacancy={vacancy} key={vacancy.id}/>
          ))} 
      </div>
        : <div style={{
        textAlign:"center",padding:"100px 0"
        }}>{t("word.no-vacancy")}</div>}
    
    </>
  );
}

export default VacancyList;
