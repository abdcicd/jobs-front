import React from "react";
import { VacancyType } from "../../../shared/types";
import styles from "./vacancy.module.scss";
import { Link } from "react-router-dom";
import image from "../../../assets/images/logo2.png";
import { IoLocationOutline } from "react-icons/io5";
import { GoLinkExternal } from "react-icons/go";
import { useLocale } from "../../../locale/LocaleProvider";
 
function VacancyCard({ vacancy }: { vacancy: VacancyType }) {
   const {t} = useLocale()
  return (
    <Link className={styles["vacancy-card"]} to={`/vacancy/${vacancy?.uuid}`}>
      <h2>{vacancy.name}</h2> 
      <p className={styles["candidates-count"]}>
          {vacancy.count
            ? t("vacancy.count").replace("{%count%}", `${vacancy.count}`)
            : t("vacancy.noone-applied")}
        </p>
      <div className={styles.info}>
      <p className={styles.status}>{vacancy.job_type}</p>
      <p className={`${styles.status} ${styles.red}`}>{vacancy.working_type}</p>
        <p>{t("word.salary")}: &nbsp; <b> {vacancy.salary}</b></p>
      </div>
      {
        vacancy.company ?   <div className={styles.company}>
        <img src={vacancy.company?.logo || image} alt="" />
        <div className={styles["company-info"]}>
          <h3>{vacancy.company?.name}</h3>
          <p>
            <IoLocationOutline /> {vacancy.company.region}, {vacancy.company.district}
          </p>
        </div>
        <GoLinkExternal />
      </div> :""
      }
    
    </Link>
  ); 
}

export default VacancyCard;
