import styles from "./info.module.scss";
import { CompanyType, LinkType } from "../../../shared/types";
import Title from "../../Title";
import { IoLocationOutline } from "react-icons/io5";
import { Link } from "react-router-dom";
import getLinkIcon from "../../../utils/getLinkIcon";
import logo from "../../../assets/images/logo2.png";

function CompanyInfo({ company }: { company: CompanyType }) {
  return (
    <div className={styles.container}>
      <div className="container">
        <img src={company.logo || logo} alt="" />
        <div className={styles.info}>
          <Title>{company.name}</Title>
          <p>
            {" "}
            <IoLocationOutline /> {company.region}, {company.district}{" "}
          </p>
        </div>
        <div className={styles.links}>
          {company.links
            .concat(
              company.phone?.map((phone) => ({ icon: "phone", url: String(phone) })) ||
                []
            )
            .map((link: LinkType) => (
              <Link to={link.url} target="_blank">
                {getLinkIcon(link.icon)} {link.url}
              </Link>
            ))}
        </div>
        <div
          className={styles.description}
          dangerouslySetInnerHTML={{ __html: company.description }}
        ></div>
      </div>
    </div>
  );
}

export default CompanyInfo;
