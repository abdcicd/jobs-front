import React from "react";
import { CompanyType } from "../../../shared/types";
import styles from "./company.module.scss";
import { FiExternalLink } from "react-icons/fi";
import { Link } from "react-router-dom";
 import logo from "../../../assets/images/download.jpg";

function CompanyCard({
  company,
  setCurrentCompany,
}: {
  company: CompanyType;
  setCurrentCompany: React.Dispatch<
    React.SetStateAction<CompanyType | undefined>
  >;
}) {
  return (
    <button className={styles.card} onClick={() => setCurrentCompany(company)}>
      <img src={company.logo ||logo} alt="" />
      <div>
        <h2 className="montserrat">{company.name}</h2>
        <p>{company.region}. {company.district}</p>
      </div>
      <Link to={`/company/${company.uuid}`}>
        <FiExternalLink color="#767F8C" size={20} />
      </Link>
    </button>
  );
}

export default CompanyCard;
