import React from "react";
import { CompanyType, MainPropType } from "../../../shared/types";
import styles from "./company.module.scss";
 import CompanyCard from "./CompanyCard";

function CompanyList({
  children,
  setCurrentCompany,
  companies,
  ...rest
}: {
  companies: CompanyType[];
  setCurrentCompany: React.Dispatch<
    React.SetStateAction<CompanyType | undefined>
  >;
} & MainPropType) {
  return (
    <div {...rest} className={styles.list}>
      {companies.map((company: CompanyType) => (
        <CompanyCard company={company} setCurrentCompany={setCurrentCompany} />
      ))}
    </div>
  );
}

export default CompanyList;
