import React from "react";
import styles from "./styles.module.scss";
import Title from "../../Title";
import { Link } from "react-router-dom";
import Button from "../../Button";
import { VacancyType } from "../../../shared/types";
import VacancyCard from "../VacancyList/VacancyCard";
import { useLocale } from "../../../locale/LocaleProvider";

function SliderVacancies({
  vacancies,
  title,
  style,
}: {
  title: string;
  vacancies: VacancyType[];
  style?: React.CSSProperties;
}) {
  const { t } = useLocale();

  return (
    <div className={styles.container} style={style}>
      {" "}
      <div className="container">
        <div className={"title-button"}>
          <Title>{t(title)}</Title>{" "}
          <Link to="/vacancies">
            <Button mode="outline" color="pink" style={{ paddingInline: 30 }}>
              {t("button.view-all")}
            </Button>
          </Link>
        </div>
        <div className={styles.cards}>
          {vacancies.map((vacancy: VacancyType) => (
            <VacancyCard vacancy={vacancy} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default SliderVacancies;
