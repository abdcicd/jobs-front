import React from "react";
import styles from "./index.module.scss";
import Title from "../../Title";
import { Link } from "react-router-dom";
import Button from "../../Button";
import Card from "./TCCard";
import Slider from "react-slick";
import { useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { CompanyType } from "../../../shared/types";
import { useLocale } from "../../../locale/LocaleProvider";

function TopCompanies() {
  const { t, lang } = useLocale();

  const { data: companies,isLoading } = useQuery(["top-companies",lang], () =>
    jwtAxios.get("/web/top-companies?pagination=1&rows=3",{ params: { lang } })
  );
  if (isLoading) {
    return <></>
  }
  
  
  const settings = {
    dots: false,
    swipe: true,
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 10000,
    swipeToSlide: true,

    slidesToShow: companies?.data?.length && companies.data.length > 3 ? 2 : companies?.data?.length,
    slidesToScroll: 1,
    responsive: [
      {
        breakpoint: 1150,
        settings: {
          slidesToShow: 2,
        },
      },
      {
        breakpoint: 700,
        settings: {
          slidesToShow: 1,
        },
      },
    ],
  };



  return (
    <div className={styles.container}>
      <div className="container">
        <div className={"title-button"}>
          <Title>
            {t("home.top.companies.title")}{" "}
            <span>{t("home.top.companies.title.span")}</span>
          </Title>{" "}
          <Link to="/companies">
            <Button mode="outline" color="pink" style={{ paddingInline: 30 }}>
              {t("button.view-all")}{" "}
            </Button>
          </Link>
        </div>
        <div className={styles.cards}>
          <Slider {...settings}>
            {companies?.data.map((company: CompanyType) => (
              <div>
                <Card company={company} />
              </div>
            ))}
          </Slider>
        </div>
      </div>
    </div>
  );
}

export default TopCompanies;
