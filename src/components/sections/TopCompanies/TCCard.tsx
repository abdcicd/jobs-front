import React from "react";
import styles from "./index.module.scss";
import image from "../../../assets/images/logo.png";
import Button from "../../Button";
import { Link } from "react-router-dom";
import { IoLocationOutline } from "react-icons/io5";
import { CompanyType } from "../../../shared/types";
import { useLocale } from "../../../locale/LocaleProvider";

function  Card({ company }: { company: CompanyType }) {  const { t } = useLocale();

  return (
    <div className={styles.card}>
      <div className={styles.top}>
        <img src={company.logo || image} alt="" />
        <div className="inter">
          <h1>{company.name}</h1>
          <p>
            {company.region ? (
              <>
                <IoLocationOutline />
                {company.region}
              </>
            ) : (
              ""
            )}
           </p>
        </div>
      </div>
      <Link to={`/vacancies?company=${company.id}`}>
        <Button color="pink" style={{ width: "100%" }}>
          {t('word.open-positions')}
        </Button>
      </Link>
    </div>
  );
}

export default Card;
