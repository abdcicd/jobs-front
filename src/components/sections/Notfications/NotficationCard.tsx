import React from "react";
import styles from "./alerts.module.scss";
import logo from "../../../assets/images/404.png";
import { Link } from "react-router-dom";

function NotficationCard() {
  return (
    <div className={styles.card}>
      <img src={logo} alt="" />
      <div>
        <h1>ADM Global Company is hiring MERN Stack Developers</h1>
        <Link to="/">Apply for a job</Link>
      </div>
    </div>
  );
}

export default NotficationCard;
