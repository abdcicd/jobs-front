import React from "react";
import styles from "./index.module.scss";
import FieldSet from "../../FieldSet";
import { useAuthData } from "../../../auth/JWTAuthProvider";
import { MONTHS } from "../../../shared/constants";
import { Table } from "antd";
import { getColumns } from "../../../pages/profile/Table/columns";
import { useLocale } from "../../../locale/LocaleProvider";

function UserInfos() {
  const { user } = useAuthData();
  const dateOfBirth = new Date(user?.date_of_birth || "");
  const { t } = useLocale();
  const columns = getColumns(t);
  const eduColumns = columns.education.slice(0, -1);
  const experienceColumns = columns.work_experience.slice(0, -1);
  const langColumns = columns.languages.slice(0, -1);
  const recommandationColumns = columns.recommandations.slice(0, -1);
  const recommenderEmployeesColumns = columns.recommender_employees.slice(
    0,
    -1
  );
  // const relativeEmployeesColumns = columns.relative_employees.slice(0, -1);

  return (
    <div className={styles.container}>
      <div className={styles.flex}>
        <FieldSet
          title={t("profile.contact-infos")}
          entities={[
            [
              `${t("profile.contact")}:`,
              user?.contact_phone || t("word.undefined"),
            ],
            [
              `${t("profile.contact2")}:`,
              user?.contact_phone_2 || t("word.undefined"),
            ],
            [`${t("profile.email")}:`, user?.email || t("word.undefined")],
          ]}
        />
        <FieldSet
          title={t("profile.addicional-infos")}
          entities={[
            [
              `${t("profile.nationality")}:`,
              user?.nationality_name || t("word.undefined"),
            ],
            [
              `${t("profile.driver-license")}:`,
              user?.driver_licence_number || t("word.undefined"),
            ],
            [`${t("profile.address")}:`, user?.address || "Nom'alum"],
          ]}
        />{" "}
      </div>{" "}
      <FieldSet
        title={t("profile.private-infos")}
        entities={[
          [
            `${t("profile.gender")}:`,
            user?.gender?.name || t("word.undefined"),
          ],
          [`${t("profile.region")}:`, user?.region_name || t("word.undefined")],
          [
            `${t("profile.district")}:`,
            user?.district_name || t("word.undefined"),
          ],
          [
            `${t("profile.place-of-birth")}:`,
            user?.place_of_birth || t("word.undefined"),
          ],
          [
            `${t("profile.citizenship")}:`,
            user?.citizenship || t("word.undefined"),
          ],
          [`${t("profile.has-car")}:`, user?.has_car ? "Ha" : "Yoq"],
          [
            `${t("profile.date-of-birth")}:`,
            user?.date_of_birth || t("word.undefined"),
          ],  [
            `${t("profile.expected_salary")}:`,
            user?.expected_salary || t("word.undefined"),
          ],
        ]}
        double
      /> 
      <FieldSet title={t("profile.education")}>
        <Table
          pagination={false}
          columns={eduColumns}
          dataSource={user?.education || []}
          scroll={{ x: 800 }}
        />
      </FieldSet>
      <FieldSet title={t("profile.work_experience")}>
        <Table
          pagination={false}
          columns={experienceColumns}
          dataSource={user?.work_experience || []}
          scroll={{ x: 800 }}
        />
      </FieldSet>{" "}
      {user?.skills?.[0] && (
        <FieldSet title={t("profile.skills")}>
          <div className={styles.skills}>
            {user?.skills.map((skill) => (
              <p>{skill}</p>
            ))}
          </div>
        </FieldSet>
      )}
      <FieldSet title={t("profile.languages")}>
        <Table
          pagination={false}
          columns={langColumns}
          dataSource={user?.languages || []}
          scroll={{ x: 800 }}
        />
      </FieldSet>{" "}
      <FieldSet title={t("profile.recommandations")}>
        <Table
          pagination={false}
          columns={recommandationColumns}
          dataSource={user?.recommandations || []}
          scroll={{ x: 800 }}
        />
      </FieldSet>{" "}
      <FieldSet title={t("profile.recommender_employees")}>
        <Table
          pagination={false}
          columns={recommenderEmployeesColumns}
          dataSource={user?.recommender_employees || []}
          scroll={{ x: 800 }}
        />
      </FieldSet>{" "}
      {/* <FieldSet title={t("profile.relative_employees")}>
        <Table pagination={false}
          columns={relativeEmployeesColumns}
          dataSource={user?.relative_employees || []}
          scroll={{ x: 800 }}
        />
      </FieldSet> */}
      {user?.hobby && (
        <FieldSet title={t("profile.hobbies")}>
          <div className={styles.skills}>{user.hobby}</div>
        </FieldSet>
      )}
      {user?.strengths && (
        <FieldSet title={t("profile.strength")}>
          <div className={styles.skills}>{user.strengths}</div>
        </FieldSet>
      )}
      {user?.criminal_responsibility && (
        <FieldSet title={t("profile.criminal-responsibility")}>
          <div className={styles.skills}>{user.criminal_responsibility}</div>
        </FieldSet>
      )}
      {user?.attending_court && (
        <FieldSet title={t("profile.attending-court")}>
          <div className={styles.skills}>{user.attending_court}</div>
        </FieldSet>
      )}
    </div>
  );
}

export default UserInfos;
