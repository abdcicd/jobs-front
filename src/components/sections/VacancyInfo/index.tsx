import React, { useEffect, useState } from "react";
import { VacancyType } from "../../../shared/types";
import styles from "./info.module.scss";
import Title from "../../Title";
import { LiaMoneyBillWaveAltSolid } from "react-icons/lia";
import { Link, useLocation, useNavigate } from "react-router-dom";
import { IoLocationOutline, IoPricetagsOutline } from "react-icons/io5";
import logo from "../../../assets/images/logo2.png";
import { Button, Modal, notification } from "antd";
import jwtAxios from "../../../auth/jwt-api";
import { ExclamationCircleTwoTone, LoadingOutlined } from "@ant-design/icons";
import { useQueryClient } from "react-query";
import { BsCalendar3, BsInstagram } from "react-icons/bs";
import { BiLogoTelegram, BiTimer } from "react-icons/bi";
import { MdOutlineArchitecture } from "react-icons/md";
import { useAuthData } from "../../../auth/JWTAuthProvider";
import { useLocale } from "../../../locale/LocaleProvider";
import {
  TelegramShareButton,
  EmailShareButton,
  FacebookShareButton,
} from "react-share";
import {
  AiOutlineCopy,
  AiOutlineFacebook,
  AiOutlineMail,
} from "react-icons/ai";

// "apply.error.username": "имя пользователя",
//   "apply.error.firstname": "имя",
//   "apply.error.lastname": "фамилия",
//   "apply.error.region_name": "провинция/город",
//   "apply.error.district_name": "район/город",
//   "apply.error.address": "адрес",
//   "apply.error.date_of_birth": "день рождения",
//   "apply.error.contact_phone": "номер телефона",
//   "apply.error.education": "место учебы",
//   "apply.error.skills": "знание",
//   "apply.error.languages": "язык",
//   "apply.error.avatar": "изображение",

const redirectPage = {
  username: "/profile/edit/information",
  firstname: "/profile/edit/information",
  lastname: "/profile/edit/information",
  region_name: "/profile/edit/information",
  address: "/profile/edit/information",
  date_of_birth: "/profile/edit/information",
  contact_phone: "/profile/edit/information",
  education: "/profile/table?page=education",
  skills: "/profile/edit/skills",
  languages: "/profile/table?page=languages",
  avatar: "/profile/edit/information",
  jshshir: "/profile/edit/information",
};

function VacancyInfo({
  vacancy,
  queryId,
}: {
  vacancy: VacancyType;
  queryId: any;
}) {
  const [api, contextHolder] = notification.useNotification();
  const [loading, setLoading] = useState(false);
  const queryClient = useQueryClient();
  const applied = vacancy.applied;
  const { isAuthenticated } = useAuthData();
  const { t } = useLocale();
  const navigate = useNavigate();
  const x = useLocation();
  console.log({ vacancy });

  const applyVacancy = async () => {
    if (!isAuthenticated) {
      notification.error({
        message: t("notification.error.vacancy.apply.login"),
      });
      navigate("/auth/signin");
      return;
    }
    const uuid = vacancy.uuid;
    setLoading(true);
    try {
      const res = await jwtAxios.post(
        `/web/profile/${applied ? "un" : ""}apply/${uuid}`
      );
      console.log(res);
      api.success({
        message: t(`notification.success.${applied ? "un" : ""}apply`),
      });

      queryClient.invalidateQueries(queryId);
    } catch (error: any) {
      const errorFields = error?.response?.data?.data?.errors || [];

      api.error({
        message: errorFields[0] ? (
          <Link to="/profile/edit-profile">
            {t(`notification.error.fields.apply`)}
          </Link>
        ) : (
          t(`notification.error.${applied ? "un" : ""}apply`)
        ),

        description: (
          <ul>
            {errorFields.map((error: string) => (
              <li>
                <Link to={redirectPage[error as keyof typeof redirectPage]}>
                  {t(`apply.error.${error}`)} 
                </Link>
              </li>
            ))}
          </ul>
        ),
      });
    }
    setLoading(false);
  };

  return (
    <div className={styles.info}>
      {contextHolder}
      <div className="container">
        <Title>{vacancy.name}</Title>
        <p className={styles["candidates-count"]}>
          {vacancy.count
            ? t("vacancy.count").replace("{%count%}", `${vacancy.count}`)
            : t("vacancy.noone-applied")}
        </p>
        <div className={styles.details}>
          <div className={styles.column}>
            <h2>{t("vacancy.job-details")}</h2>
            {vacancy.salary ? (
              <div className={styles.pay}>
                <LiaMoneyBillWaveAltSolid size={20} />
                <div>
                  <h3>{t("word.salary")}</h3>
                  <p>
                    <span>{vacancy.salary}</span>
                  </p>
                </div>
              </div>
            ) : (
              ""
            )}
            {vacancy.schedule ? (
              <div className={styles.pay}>
                <BsCalendar3 />
                <div>
                  <h3>{t("vacancy.working-days")}</h3>
                  <p>
                    <span>{vacancy.schedule}</span>
                  </p>
                </div>
              </div>
            ) : (
              ""
            )}
            {/* {vacancy.start_date || vacancy.end_date.replaceAll("-", ".") ? (
              <div className={styles.pay}>
                <BiTimer size={23} />
                <div>
                  <h3>{t("vacancy.period")}</h3>
                  <p>
                    <span>
                      {vacancy.start_date.replaceAll("-", ".")}
                      {" - " + vacancy.end_date.replaceAll("-", ".")}
                    </span>
                  </p>
                </div>
              </div>
            ) : (
              ""
            )}{" "} */}
            <div className={styles.pay}>
              <MdOutlineArchitecture size={20} />
              <div>
                <h3>{t("vacancy.job-type")}</h3>
                <p>
                  {vacancy.job_type ? <span>{vacancy.job_type}</span> : ""}
                  {vacancy.job_type ? <span>{vacancy.working_type}</span> : ""}
                </p>
              </div>
            </div>
            {vacancy.start_date || vacancy.end_date.replaceAll("-", ".") ? (
              <div className={styles.pay}>
                <IoPricetagsOutline />
                <div>
                  <h3>{t("vacancy.tags")}</h3>
                  <p>
                    {vacancy.tags.map((tag) => (
                      <span>{tag}</span>
                    ))}
                  </p>
                </div>
              </div>
            ) : (
              ""
            )}
            <Button type={"primary"} onClick={applyVacancy}>
              {loading ? (
                <>
                  <LoadingOutlined spin />
                  {applied ? t("button.unapplying") : t("button.applying")}
                </>
              ) : applied ? (
                t("button.unapply")
              ) : (
                t("button.apply")
              )}
            </Button>
            <div className={styles["share-buttons"]}>
              <button
                onClick={() => {
                  navigator.clipboard.writeText(window.location.href).then(() =>
                    notification.success({
                      message: t("notification.success.copy"),
                    })
                  );
                }}
                style={{
                  backgroundColor: "white",
                  color: "black !important",
                  borderColor: "black",
                  borderRadius: 5,
                }}
              >
                <AiOutlineCopy />
              </button>{" "}
              <TelegramShareButton
                url={window.location.href}
                style={{ borderRadius: 5, backgroundColor: "#1c93e3" }}
              >
                <BiLogoTelegram />{" "}
              </TelegramShareButton>
              <FacebookShareButton
                url={window.location.href}
                style={{ borderRadius: 5, backgroundColor: "#1877f2" }}
              >
                <AiOutlineFacebook />{" "}
              </FacebookShareButton>
              <EmailShareButton
                url={window.location.href}
                style={{ borderRadius: 5, backgroundColor: "black" }}
              >
                <AiOutlineMail />{" "}
              </EmailShareButton>
            </div>
          </div>
          <div className={styles.column}>
            {vacancy.company ? (
              <>
                <h2>{t("word.company")}</h2>
                <Link
                  to={`/company/${vacancy.company.uuid}`}
                  className={styles.company}
                >
                  <img src={vacancy.company.logo || logo} alt="" />
                  <div>
                    <h2>{vacancy.company.name}</h2>
                    <p>
                      <IoLocationOutline /> {vacancy.company.region},{" "}
                      {vacancy.company.district}
                    </p>
                  </div>
                </Link>
              </>
            ) : (
              ""
            )}
          </div>
        </div>
        <div
          className={`${styles.description} montserrat`}
          dangerouslySetInnerHTML={{ __html: vacancy.description }}
        />{" "}
        <br />
        <br /> <Title size="small">{t("vacancy.requirements")}</Title>{" "}
        <div
          className={`${styles.description} montserrat`}
          dangerouslySetInnerHTML={{ __html: vacancy.requirements }}
        />
        <br />
        <br />
        <Title size="small">{t("vacancy.tasks")}</Title>{" "}
        <div
          className={`${styles.description} montserrat`}
          dangerouslySetInnerHTML={{ __html: vacancy.tasks }}
        />
        <br />
        <br />
        <Title size="small">{t("vacancy.conditions")}</Title>{" "}
        <div
          className={`${styles.description} montserrat`}
          dangerouslySetInnerHTML={{ __html: vacancy.conditions }}
        />
      </div>
    </div>
  );
}

export default VacancyInfo;
