import React, { useState, useMemo } from "react";
import Title from "../../Title";
import { Button, Select, Table } from "antd";
import styles from "./table.module.scss";
import { recentActions } from "../../../data/recentActions";
import { RecentActionType, VacancyType } from "../../../shared/types";
import { Link } from "react-router-dom";
import { GoLinkExternal } from "react-icons/go";
import logo from "../../../assets/images/logo.png";
import { useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { useLocale } from "../../../locale/LocaleProvider";
import EventModal from "../../Modals/EventModal";

function ActionsTable() {
  const [currentStatus, setCurrentStatus] = useState<"" | number>("");
  const { t ,lang} = useLocale();
  const { data: statuses } = useQuery(["statuses",lang], () =>
    jwtAxios.get("/enum/list-application-status-types",{ params: { lang } })
  );
  const { data: vacancies } = useQuery(["recent-vacancies",lang], () =>
    jwtAxios.get("/web/profile/vacancies",{ params: { lang } })
  );

 

  const filteredVacancies = useMemo(() => {
    const temporaryVacances = vacancies?.data || [];
    if (currentStatus === "") {
      return temporaryVacances;
    }

    return temporaryVacances.filter(
      (vacancy: {
        application_status: {
          type: number;
          name: string;
        };
      }) => vacancy.application_status.type === currentStatus
    );
  }, [vacancies, currentStatus]);

  const columns = [
    {
      title: t("word.company"),
      dataIndex: "company",
      key: "company",
      width: 250,
      render(data: any, object: { vacancy_uuid: string }) {
        return (
          <Link to={`/vacancy/${object.vacancy_uuid}`}>
            <div className={styles.job}>
              <img src={logo} alt="" />
              <div>
                <h3>{data?.name}</h3>
                <p>{data?.district}</p>
              </div>
            </div>
          </Link>
        );
      },
    },
    {
      title: t("word.vacancy"),
      dataIndex: "name",
      render(data: string, object: { vacancy_uuid: string }) {
        return (
          <Link
            to={`/vacancy/${object.vacancy_uuid}`}
            style={{ color: "unset" }}
          >
            {data}
          </Link>
        );
      },
      key: "age",
    },
    {
      title: t("overview.applied-date"),
      dataIndex: "created_at",
      render(data: string) {
        return (
          <>
            {data?.substr(0, 10)} {data?.substr(11, 5)}
          </>
        );
      },
      key: "age",
    },
    {
      title: t("word.salary"),
      dataIndex: "salary",
      key: "salary",
    },
    {
      title: t("word.status"),
      dataIndex: "application_status",
      key: "status",
      render: (
        status: { type: number; name: string },
        obj:any
      ) => (
        <>
          <EventModal events={obj.events}>
            {" "}
            <Button type="primary" ghost>
              {  t(`status.${status.type}`)}
            </Button>
          </EventModal>
        </>
      ),
    },
    // {
    //   title: t("word.link"),
    //   dataIndex: "vacancy_uuid",
    //   key: "link",
    //   render(data: number) {
    //     return (
    //       <Link to={"/vacancy/" + data}>
    //         <GoLinkExternal />
    //       </Link>
    //     );
    //   },
    // },
  ];
 
  return (
    <div className={styles["table-component"]}>
      <div className={styles["title-component"]}>
        <Title>{t("overview.recent-actions")}</Title>
        <Select
          value={currentStatus}
          style={{ width: 120 }}
          options={[
            { value: "", label: t("word.all") },
            ...(statuses?.data.map(
              (status: { code: number; name: string }) => ({
                value: status.code,
                label: t(`status.${status.code}`)  ,
              })
            ) || []),
          ]}
          onChange={setCurrentStatus}
        />
      </div>
      <Table
        dataSource={filteredVacancies}
        columns={columns}
        scroll={{ x: 800 }}
      />
    </div>
  );
}

export default ActionsTable;
