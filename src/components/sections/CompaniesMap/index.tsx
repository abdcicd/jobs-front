import React, { useState } from "react";
import {
  MapContainer,
  TileLayer,
  Popup,
  Marker,
  useMapEvent,
  useMap,
} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";
 import styles from "./map.module.scss";
import { CompanyType } from "../../../shared/types";

function CompaniesMap({
  currentCompany,
  setCurrentCompany,
  companies,
}: {
  companies: CompanyType[];
  currentCompany?: CompanyType;
  setCurrentCompany: React.Dispatch<
    React.SetStateAction<CompanyType | undefined>
  >;
}) {
  const ChangeMapView = ({ coords }: { coords: [number, number] }) => {
    const map = useMap();
    map.flyTo(coords, 17, {
      duration: 1,
    });
    return null;
  };

  return (
    <MapContainer
      center={[40.615061, 64.827988]}
      zoom={6}
      scrollWheelZoom={true}
      style={{
        width: "100%",
        height: "100%",
        background: "white",
        zIndex: 1,
      }}
    
    >
      <TileLayer
        attribution='&copy; <a href="https://nextech.uz">Nextech</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {currentCompany && <ChangeMapView coords={[currentCompany.latitude,currentCompany.longitude]} />}{" "}
      {companies.map((company: CompanyType) => (
        <Marker
          position={[company.latitude, company.longitude]}
          icon={
            new L.Icon({
              iconUrl: require("../../../assets/images/marker-icon.png"),
              iconRetinaUrl: require("../../../assets/images/marker-icon.png"),
              iconSize: [25, 40],
              className: styles.marker,
            })
          }
          eventHandlers={{
            click: () => {
              setCurrentCompany(company);
            },
          }}
        >
          <Popup>{company.name}</Popup>
        </Marker>
      ))}
    </MapContainer>
  );
}

export default CompaniesMap;
