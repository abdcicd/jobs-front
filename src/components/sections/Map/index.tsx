import { LatLngExpression } from "leaflet";
import React from "react";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import L from "leaflet";
import styles from "./map.module.scss";
import { MainPropType } from "../../../shared/types";

function Map({ latlng,style }: { latlng: LatLngExpression } & MainPropType) {
  return (
    <MapContainer
      center={latlng}
      zoom={13}
      scrollWheelZoom={false}
      style={{
        width: "100%",
        height: "100vh",
        maxHeight:500,
        zIndex: 1,...style
      }}
    >
      <TileLayer
        attribution='&copy; <a href="https://nextech.uz">Nextech</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker
        position={latlng}
        icon={
          new L.Icon({
            iconUrl: require("../../../assets/images/marker-icon.png"),
            iconRetinaUrl: require("../../../assets/images/marker-icon.png"),
            iconSize: [25, 40],
            className: styles.marker,
          })
        }
      ></Marker>
    </MapContainer>
  );
}

export default Map;
