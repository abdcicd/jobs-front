import React from "react";
import styles from "./statics.module.scss";

function SCard({
  data,
}: {
  data: { icon: React.ReactNode; value: number | string; variable: string };
}) {
  return (
    <div className={styles.card}>
<div className={styles.icon}>      {data.icon}
</div>      <div className="inter">
        <h1>{data.value}</h1>
        <p>{data.variable}</p>
      </div>
    </div>
  );
}

export default SCard;
