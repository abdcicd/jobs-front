import React from "react";
import { BsBuildings } from "react-icons/bs";
import { FaSuitcase, FaUserTie } from "react-icons/fa";
import { MdFiberNew } from "react-icons/md";
import SCard from "./SCard";
import styles from "./statics.module.scss";
import { useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { useLocale } from "../../../locale/LocaleProvider";
const fakeData = {
  companies: 123,
  applications: 12,
  vacancies: 3212,
  live_jobs: 3212,
};

function Statistics() {  const { t, lang } = useLocale();

  const { data } = useQuery(["info-home",lang], () => jwtAxios.get("/web/info-home",{ params: { lang } }));
  const statistics: {
    companies: number;
    applications: number;
    vacancies: number;
    live_jobs: number;
  } = data?.data  || {
    companies: 0,
    applications: 0,
    vacancies: 0,
    live_jobs: 0,
  };


 
  return (
    <div className={styles.container}>
      <div className="container">
        <SCard
          data={{
            icon: <FaSuitcase />,
            value: statistics.live_jobs,
            variable: t("total.live-jobs"),
          }}
        />
        <SCard
          data={{
            icon: <BsBuildings />,
            value: statistics.companies,
            variable:  t("total.companies"),
          }}
        />{" "}
        <SCard
          data={{
            icon: <FaUserTie />,
            value: statistics.applications,
            variable:  t("total.candidates")
          }}
        />
        <SCard
          data={{
            icon: <MdFiberNew />,
            value: statistics.vacancies,
            variable: t("total.all-jobs"),
          }}
        />
      </div>
    </div>
  );
}

export default Statistics;
