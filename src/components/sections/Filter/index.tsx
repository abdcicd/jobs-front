import React, { useState } from "react";
import { BiSearch } from "react-icons/bi";
import { GiSettingsKnobs } from "react-icons/gi";
import Button from "../../Button";
import styles from "./filter.module.scss";
import { FilterType, VacancyType } from "../../../shared/types";
import { useLocale } from "../../../locale/LocaleProvider";
import { useQuery } from "react-query";
import jwtAxios from "../../../auth/jwt-api";
import { DatePicker, Select } from "antd";
import { dateFormat } from "../../../shared/constants";
import { useLocation } from "react-router-dom";

const changeFieldDate = (
  setFilter: React.Dispatch<React.SetStateAction<FilterType>>,
  field: string,
  value: [any, any] | null
) => {
  if (!value) {
    return setFilter((filter: FilterType) => ({ ...filter, [field]: "" }));
  }

  setFilter((filter) => ({
    ...filter,
    [field]: `${value?.[0].format(dateFormat)}|${value?.[1].format(
      dateFormat
    )}`,
  }));
};

function Filter({
  setFilters,
  filters,
  company,
}: {
  setFilters: React.Dispatch<React.SetStateAction<FilterType>>;
  filters: FilterType;
  company: null | string;
}) {
  const { t, lang } = useLocale();
  const [open, setOpen] = useState(!!company);

  const { data: companies } = useQuery(["company",lang], () =>
    jwtAxios.get("/web/list-companies",{ params: { lang } })
  );
  const { data: jobTypes } = useQuery(["list-job-types",lang], () =>
    jwtAxios.get("/enum/list-job-types",{ params: { lang } })
  );
  const { data: worktypes } = useQuery(["list-wroking-types",lang], () =>
    jwtAxios.get("/enum/list-working-types",{ params: { lang } })
  );

  return (
    <>
      <form
        action=""
        className={styles.form}
        onSubmit={(e) => {
          e.preventDefault();

          const formdata = new FormData(e.currentTarget);
          const data = Object.fromEntries(formdata.entries());
          setFilters((prev) => ({
            ...prev,
            page: 1,
            s: `${data.search || ""}`,
          }));
        }}
      >
        <BiSearch size={30} />
        <input
          type="text"
          name="search"
          placeholder={t("word.search") + "..."}
          defaultValue={filters.s}
        />
        <Button
          color="grey"
          type="button"
          className={styles.filterBtn}
          onClick={() => setOpen(!open)}
        >
          <GiSettingsKnobs size={20} /> <p>{t("button.filter")} </p>
        </Button>
        <Button className={styles.searchBtn}>
          {" "}
          <BiSearch size={20} /> <p>{t("button.find-job")}</p>
        </Button>
      </form>
      <div
        className={styles.filters}
        style={{ display: open ? "grid" : "none" }}
      >
        <span>
          <p>{t("word.companies")}</p>
          <Select
            defaultValue={filters.company ? +filters.company : ""}
            options={[
              { value: "", label: t("word.all") },
              ...(companies?.data?.data?.map(
                (company: { id: string | number; name: string }) => ({
                  value: company.id,
                  label: company.name,
                })
              ) || []),
            ]}
            onChange={(value) => setFilters({ ...filters, company: value })}
            style={{ width: "100%" }}
          />
        </span>{" "}
        <span>
          <p>{t("vacancy.job-type")}</p>
          <Select
            defaultValue={""}
            options={[
              { value: "", label: t("word.all") },
              ...(jobTypes?.data?.map(
                (jobType: { code: string | number; name: string }) => ({
                  value: jobType.code,
                  label: jobType.name,
                })
              ) || []),
            ]}
            onChange={(value) => setFilters({ ...filters, job_type: value })}
            style={{ width: "100%" }}
          />
        </span>
        <span>
          <p>{t("vacancy.working-type")}</p>
          <Select
            defaultValue={""}
            options={[
              { value: "", label: t("word.all") },
              ...(worktypes?.data?.map(
                (worktype: { code: string | number; name: string }) => ({
                  value: worktype.code,
                  label: worktype.name,
                })
              ) || []),
            ]}
            onChange={(value) =>
              setFilters({ ...filters, working_type: value })
            }
            style={{ width: "100%" }}
          />
        </span>
        {/* <span>
          <p>{t("profile.start-date")}</p>
          <DatePicker.RangePicker
            size={"middle"}
            style={{ width: "100%" }}
            onChange={(value) =>
              changeFieldDate(setFilters, "start_date", value)
            }
          />
        </span>{" "} */}
      </div>
    </>
  );
}

export default Filter;
