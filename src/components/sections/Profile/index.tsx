import React, { useState } from "react";
import { Link } from "react-router-dom";
import MButton from "../../Button";
import styles from "./index.module.scss";
import image from "../../../assets/images/avatar.jpg";
import Title from "../../Title";
import { useAuthData } from "../../../auth/JWTAuthProvider";
import { MONTHS } from "../../../shared/constants";
import { useLocale } from "../../../locale/LocaleProvider";
import { download } from "../../../utils/download";
import { Button, Space, notification } from "antd";
import jwtAxios from "../../../auth/jwt-api";

function Profile() {
  const { user } = useAuthData();
  const job = user?.work_experience?.filter((job) => job.to_present)[0];
  const dateOfBirth = user?.date_of_birth && new Date(user?.date_of_birth);
  const { t } = useLocale();
  const userSmallInfos = [
    ...(user?.gender.name ? [user?.gender.name] : []),
    ...(dateOfBirth
      ? [
          ` ${new Date().getFullYear() - dateOfBirth.getFullYear()} ${t(
            "word.years-old"
          )}, ${t("word.born-on")} ${user?.date_of_birth}`,
        ]
      : []),
  ];
  const [isPDFLoading, setIsPDFLoading] = useState(false);
  console.log({ user });

  const handleDownload = async () => {
    setIsPDFLoading(true);
    try {
      const res = await jwtAxios.get(`web/profile-cv`, {
        responseType: "blob",
      });
      console.log({ res });

      if (res.status === 201) {
        setTimeout(handleDownload, 3000);
        return;
      }
      download(
        `${user?.firstname || ""} ${user?.lastname || ""} (${
          user?.username || ""
        }).pdf`,
        { binarydata: res.data }
      );
    } catch (error: any) {
      notification.error({
        message: error.message,
      });
      console.log(error);
    }
    setIsPDFLoading(false);
  };

  return (
    <div className={styles.container}>
      <div className={styles.top}>
        <img src={user?.avatar || image} alt="" />
        <div>
          <Title size="small">{`${user?.firstname} ${user?.lastname}  `}</Title>
          <p>
            {job
              ? `${job.position}, ${job.organization}`
              : t("profile.unemployed")}
          </p>
          <h2>{userSmallInfos.join(", ")}</h2>
        </div>
      </div>
      <Space style={{
        display:"flex"
        ,justifyContent:"flex-end"
      }}>
        <Button loading={isPDFLoading} onClick={handleDownload}>PDF</Button>
        <Link
          to="/profile/edit-profile"
          style={{ width: "calc( 200px + 5vw)", maxWidth: "100%" }}
        >
          <MButton color="pink" style={{ width: "100%" }}>
            {t("button.edit-profile")}
          </MButton>
        </Link>
      </Space>
    </div>
  );
}

export default Profile;
