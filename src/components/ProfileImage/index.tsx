import React, { useState } from "react";
import avatar from "../../assets/images/avatar.jpg";
import styles from "./index.module.scss";
import { BsCameraFill } from "react-icons/bs";
import jwtAxios from "../../auth/jwt-api";
import { UserDataType, useAuthAction } from "../../auth/JWTAuthProvider";
import { UserType } from "../../shared/types";
import { Spin, notification } from "antd";
import { useLocale } from "../../locale/LocaleProvider";

function ProfileImage({ image }: { image?: string }) {
  const { setUserData } = useAuthAction();

  const [loading, setLoading] = useState<boolean>(false);
  const [api, contextHolder] = notification.useNotification();
  const { t } = useLocale();
  const handleImageChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    try {
      const file = e.target.files?.[0];
      if (!file) {
        return;
      }
      const formData = new FormData();
      formData.append("file", file);

      let user: UserType;
      setLoading(true);
      const data = await jwtAxios.post("/web/profile/file", formData, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
 
      setUserData((prev: UserDataType) => ({
        ...prev,
        user: { ...prev.user, avatar: data.data.avatar },
      }));
      setLoading(false);
      api.success({ message: t(`notification.success.image-update`) });
    } catch (error: any) {
      setUserData((prev: UserDataType) => ({ ...prev, isLoading: false }));
      setLoading(false);
      // api.error({
      //   message:
      //     error.response?.data?.errors?.file?.[0] ||
      //     error.response?.data?.message ||
      //     t(`notification.error.image-update`),
      // });
      console.log(error);
    }
  };

  return (
    <>
      <label className={styles.avatar} htmlFor="file">
        <Spin spinning={loading}>
          <img src={image || avatar} alt="" />
        </Spin>
        <div className={styles.icon}>
          <BsCameraFill />
        </div>
        <input
          type="file"
          id="file"
          onChange={handleImageChange}
          accept=".jpg, .jpeg, .png"
        />
      </label>
      {contextHolder}
    </>
  );
}

export default ProfileImage;
