import React from "react";
import { useLocale } from "../../locale/LocaleProvider";
import { Button, Dropdown } from "antd";
import { IoLanguageOutline } from "react-icons/io5";
import styles from "./index.module.scss"
import { BsGlobe2 } from "react-icons/bs";
import { useAuthAction } from "../../auth/JWTAuthProvider";

function LanguageSwitcher() {
  const { setLocaleLanguage, t, lang } = useLocale();
  const {refetchProfile} = useAuthAction()
  const handleMenuClick = (data: { key: string }) => {
    setLocaleLanguage(data.key);
    refetchProfile()
  };
  const items = [
    {
      label: "O'zbekcha",
      key: "uz",
    },
    {
      label: "Русский",
      key: "ru",
    },
  ];
  const menuProps = {
    items,
    onClick: handleMenuClick,
  };
  return (
    <>
      <Dropdown menu={menuProps}>
        <div className={styles["switcher"]}
        >
           <BsGlobe2 size={20} /> 
           <p>{t(`word.${lang}`)}</p>
        </div>
      </Dropdown>
    </>
  );
}

export default LanguageSwitcher;
