import React, { CSSProperties, useEffect } from "react";
import AuthLayout from "./AuthLayout";
import { useAuthData } from "../auth/JWTAuthProvider";
import { useLocation } from "react-router-dom";
import ProfileLayout from "./ProfileLayout";
import MainLayout from "./MainLayout";
import { authorizedStructure, unAuthorizedStructure } from "../pages";
import useRouteGenerator from "../utils/routeGenerator";
import { Spin } from "antd";
import { titles } from "../shared/constants";
import { useLocale } from "../locale/LocaleProvider";

function AppLayout() {
  const route = useLocation();
  const { user, isAuthenticated, isLoading } = useAuthData();
  const { t } = useLocale();
  const routes = useRouteGenerator(
    isAuthenticated ? authorizedStructure.routes : unAuthorizedStructure.routes
  );

  const { pathname } = useLocation();

  useEffect(() => {
    const key = pathname.split("/")[1];

    const title = t(`page.title.${key}`);

    if (!title) {
      document.title = `ADM Global`;
      return;
    }

    document.title = `ADM Global - ${title}`;

    window.scrollTo(0, 0);
  }, [pathname, t]);

  // Check if this route for auth layout
  const isAuthLayout = route.pathname.startsWith("/auth");

  // Check if this route for profile layout
  const isProfileLayout = route.pathname.startsWith("/profile");

  return (
    <div style={{ "--padding-top": user?.jshshir ? "70px" : "120px" } as CSSProperties}>
      {isAuthLayout ? (
        <AuthLayout>{routes}</AuthLayout>
      ) : isLoading ? (
        <Spin
          spinning={true}
          size="large"
          style={{
            height: "100vh",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
          }}
        ></Spin>
      ) : isProfileLayout ? (
        <ProfileLayout>{routes}</ProfileLayout>
      ) : (
        <MainLayout>{routes}</MainLayout>
      )}
    </div>
  );
}

export default AppLayout;
