import { Link, NavLink, NavLinkProps } from "react-router-dom";
import styles from "./navbar.module.scss";
import React, { useState } from "react";
import logo from "../../../assets/images/logo.png";
import Button from "../../../components/Button";
import { PiUserCircleLight } from "react-icons/pi";
import { useAuthData } from "../../../auth/JWTAuthProvider";
import { RxCross1, RxHamburgerMenu } from "react-icons/rx";
import { navLinks } from "../../../pages/routesConfig";
import { useLocale } from "../../../locale/LocaleProvider";
import LanguageSwitcher from "../../../components/LanguageSwitcher";

function Navbar() {
  const { isAuthenticated, user } = useAuthData();
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const { t } = useLocale();
  return (
    <>
    {!user?.jshshir && <div className={styles.required}  >
 <Link to="/profile/edit/information" dangerouslySetInnerHTML={{__html:t("main.add.required")}}></Link>
    </div>}
      <nav className={styles.navbar} style={{ "top": user?.jshshir ? "0px" : "50px" } }>
        <div className="container">
          <div className={styles.left}>
            <Link className={styles.image} to="/home">
              <img src={logo} alt="" />
            </Link>
            {navLinks.map((link) => (
              <NavLink
                className={({ isActive }) =>
                  [
                    styles.desktop,
                    "underline-animation",
                    isActive ? styles.active : null,
                  ].join(" ")
                }
                {...link}
               >{t(link.children)}</NavLink>
            ))}
          </div>
          <div className={styles.right}> <LanguageSwitcher/>
            {isAuthenticated ? (
              <Link to="profile/personal-infos">
                {user?.avatar ? (
                  <img
                    src={user.avatar}
                    alt=""
                    style={{
                      width: 45,
                      aspectRatio: "1/1",
                      objectFit: "cover",
                      borderRadius: "50%",
                    }}
                  />
                ) : (
                  <PiUserCircleLight size={30} />
                )}
              </Link>
            ) : (
              <>
                <Link className={styles.desktop} to="/auth/enter-phone">
                  <Button mode="outline">{t('button.register')}</Button>{" "}
                </Link>
                <Link className={styles.desktop} to="/auth/signin">
                  <Button>{t('button.login')}</Button>{" "}
                </Link>
              </>
            )}
            <RxHamburgerMenu
              size={30}
              className={styles.menuBtn}
              onClick={() => setIsMenuOpen(true)}
            />
          </div>
        </div>
      </nav>
      <div
        className={`${styles.menu} ${isMenuOpen ? styles.active : ""}`}
        onClick={() => setIsMenuOpen(false)}
      >
        <RxCross1 className={styles.xBtn} />
        {navLinks.map((link) => (
          <Link {...link}>
            <p>{t(link.children)}</p>
          </Link>
        ))}
        {isAuthenticated ? (
          ""
        ) : (
          <div className={styles.buttons}>
            <Link to="/auth/enter-phone">
              <Button mode="outline">{t("button.register")}</Button>{" "}
            </Link>
            <Link to="/auth/signin">
              <Button>{t("button.login")}</Button>{" "}
            </Link>
          </div>
        )}
      </div>
    </>
  );
}

export default Navbar;
