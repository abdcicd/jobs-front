import {
  AiFillPhone,
  AiOutlineAlert,
  AiOutlineEdit,
  AiOutlineEye,
  AiOutlineInfoCircle,
} from "react-icons/ai";
import { BsFillPhoneFill } from "react-icons/bs";
import { FaLock } from "react-icons/fa";

export const menuConfig = [
  {
    label: "sidebar.applications",
    link: "/profile/applications",
    icon: <AiOutlineEye />,
  },
  {
    label: "sidebar.personal-infos",
    link: "/profile/personal-infos",
    icon: <AiOutlineInfoCircle />,
  },
  // {
  //   label: "Notifications",
  //   link: "/profile/job-alert",
  //   icon: <AiOutlineAlert />,
  // },
  {
    label: "sidebar.edit-profile",
    link: "/profile/edit-profile",
    icon: <AiOutlineEdit />,
  },
  {
    icon: <FaLock size={14} />,
    label: "profile.change-password",
    link: "/profile/change-password",
  },
  {
    icon: <BsFillPhoneFill size={14} />,
    label: "profile.change-phone",
    link: "/profile/change-phone",
  },
  {
    icon: <BsFillPhoneFill size={14} />,
    label: "profile.telegram-bot",
    target: "_blank",
    link: "https://t.me/admjobs_bot",
  },
];
