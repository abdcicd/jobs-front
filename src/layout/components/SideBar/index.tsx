import   {   useState } from "react";
import {   NavLink } from "react-router-dom";
import styles from "./bar.module.scss";
import { useAuthAction } from "../../../auth/JWTAuthProvider";
import { BiLogOut } from "react-icons/bi";
import { RxCross1, RxHamburgerMenu } from "react-icons/rx";
import { menuConfig } from "..";
import { useLocale } from "../../../locale/LocaleProvider";

function SideBar({
  hidden,
  setHidden,
}: {
  hidden: boolean;
  setHidden: React.Dispatch<React.SetStateAction<boolean>>;
}) {
  const { logout } = useAuthAction();

  const { t } = useLocale();

  return (
    <>
      <aside className={`${styles.bar} ${hidden || styles.open}`}>
        <a
          onClick={() => setHidden((prev) => !prev)}
          style={{ justifyContent: "space-between" }}
        >
          <RxHamburgerMenu
            size={20}
            className={styles.menuBtn}
            opacity={hidden ? 1 : 0}
          />
          <RxCross1 size={20} className={styles.menuBtn} />
        </a>
        {menuConfig.map((link) => (
          <NavLink
            to={link.link}
            target={link.target}
            className={({ isActive }) =>
              [styles.link, isActive ? styles.active : null].join(" ")
            }
          >
            {link.icon}
            <p> {t(link.label)}</p>{" "}
          </NavLink>
        ))}

        <NavLink to={"/"} onClick={logout}>
          <BiLogOut /> <p>{t("sidebar.logout")}</p>{" "}
        </NavLink>
      </aside>{" "}
    </>
  );
}

export default SideBar;
