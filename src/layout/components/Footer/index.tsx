import styles from "./footer.module.scss";
import React from "react";
import logo from "../../../assets/images/logo2.png";
import { Link } from "react-router-dom";
import { BsFacebook, BsTelegram, BsYoutube } from "react-icons/bs";
import { AiFillInstagram } from "react-icons/ai";
import { useLocale } from "../../../locale/LocaleProvider";

function Footer() {
  const { t } = useLocale();

  return (
    <footer className={styles.footer}>
      <div className="container">
        <Link to="" className={styles.logo}>
          <img src={logo} alt="" />{" "}
        </Link>
        <div className={styles.row}>
          <h1>{t("word.adm-jizzakh")}</h1>
          <Link to="/home">{t("footer.home")}</Link>
          <Link to="/vacancies">{t("footer.vacancies")}</Link>
          <Link to="/companies">{t("footer.companies")}</Link>
        </div>
        <div className={styles.row}>
          <h1>{t("footer.contact-us")}</h1>
          {/* <Link to="">Contact</Link> */}
          <div className={styles.icons}>
            <Link
              to="https://www.facebook.com/profile.php?id=100094899460231"
              target="_blank"
            >
              <BsFacebook size={25} />
            </Link>
            <Link
              to="https://www.instagram.com/adm_jizzakh_hr/?next=%2F"
              target="_blank"
            >
              <AiFillInstagram size={27} />
            </Link>
            <Link
              to="https://www.youtube.com/watch?v=As1IRWPVDR8&list=PLT4Rm97168BTPKv9kFRu6gWITO8uP3deK"
              target="_blank"
            >
              <BsYoutube size={25} />
            </Link>
            <Link to="https://t.me/ADM_Jizzakh_HR" target="_blank">
              <BsTelegram size={25} />
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;
