import React from 'react'
import Navbar from '../components/Navbar'
import Footer from '../components/Footer'
import useRouteGenerator from '../../utils/routeGenerator'
import {   unAuthorizedStructure } from '../../pages'
import { RouteType } from '../../shared/types'
 
function AuthLayout({children}:{children:React.ReactNode}) {
     return (
        <div className='layout'>  {children} </div>
    )
}


export default AuthLayout

 