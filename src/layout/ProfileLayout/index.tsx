import React, { useState } from "react";
 import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
import SideBar from "../components/SideBar";
import styles from "../index.module.scss";
import BreadCrump from "../../components/BreadCrump";
import { useLocale } from "../../locale/LocaleProvider";

function ProfileLayout({ children }: { children: React.ReactNode }) {  const {t} = useLocale()
const [hidden, setHidden] = useState<boolean>(window.innerWidth < 800);

  return (
    <div className={styles.layout}>
      <Navbar />
      <div className={styles.sidebar}>
        <SideBar  {...{hidden, setHidden}}/>
        <div className={styles.content} style={{width:`calc(100% - ${hidden ? 50 : 230}px)`,transition:"0.3s"}}>
          <div>
            <BreadCrump
              paths={[
                { label: t("breadcrump.profile"), link: "/profile/overview" },
              ]}
            />
            {children}{" "}
          </div>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default ProfileLayout;
