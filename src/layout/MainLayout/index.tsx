import React from "react";
 import Navbar from "../components/Navbar";
import Footer from "../components/Footer";
 import { useLocation } from "react-router-dom";
import { errorRouteConfig } from "../../pages/errorPages";
import { RouteType } from "../../shared/types";
import styles from "../index.module.scss";

function MainLayout({ children }: { children: React.ReactNode }) {
  const route = useLocation();
 

  return (
    <div className={styles.layout}> 
      <Navbar />

      {children}

      {errorRouteConfig.find((errRoute: RouteType) => {
        return errRoute.path === route.pathname ;
      } ) || route.pathname === "/companies"  ? (
        ""
      ) : (
        <Footer />
      )}
    </div>
  );
}

export default MainLayout;
