# front-admin



## Getting started
To make it easy for you to get started with project, here's a list of recommended next steps.

 
## Install project to your local directory
```
git clone origin https://gitlab.com/admautdevs/adm/jobs/front-admin.git <directory name>
``` 

## Run project in npm
```
npm install   
npm start   
```

## Run project in yarn
```
yarn
yarn run start   
``` 



## Build project  
```
yarn run build
npm run build   
``` 


## Authors
This project done by t.me/fazliddinDEVdm 
